<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CityCollection $cityCollection
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Editar cidade para coleta'), ['action' => 'edit', $cityCollection->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Excluir cidade para coleta'), ['action' => 'delete', $cityCollection->id], ['confirm' => __('Tem certeza que deseja excluir a empresa para coleta # {0}?', $cityCollection->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar cidades para coleta'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nova cidade para coleta'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Listar cidades'), ['controller' => 'Cities', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nova cidade'), ['controller' => 'Cities', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Listar empresas'), ['controller' => 'Companies', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nova empresa'), ['controller' => 'Companies', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="cityDeliveries view large-9 medium-8 columns content">
    <h3><?= h('Coleta em '.$cityCollection->city->name.' - '.$cityCollection->city->state->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Cidade') ?></th>
            <td><?= $cityCollection->has('city') ? $this->Html->link($cityCollection->city->name, ['controller' => 'Cities', 'action' => 'view', $cityCollection->city->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Estado') ?></th>
            <td><?= $cityCollection->city->has('state') ? $this->Html->link($cityCollection->city->state->name, ['controller' => 'States', 'action' => 'view', $cityCollection->city->state->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Empresa') ?></th>
            <td><?= $cityCollection->has('company') ? $this->Html->link($cityCollection->company->name, ['controller' => 'Companies', 'action' => 'view', $cityCollection->company->id]) : '' ?></td>
        </tr>
    </table>
</div>
