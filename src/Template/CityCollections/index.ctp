<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CityCollection[]|\Cake\Collection\CollectionInterface $cityCollections
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Nova cidade para coleta'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Listar cidades'), ['controller' => 'Cities', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nova cidade'), ['controller' => 'Cities', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Listar empresas'), ['controller' => 'Companies', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nova empresa'), ['controller' => 'Companies', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="cityCollections index large-9 medium-8 columns content">
    <h3><?= __('Cidades para coleta') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('city_id',['label'=>'Cidade']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('company_id',['label'=>'Empresa']) ?></th>
                <th scope="col" class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($cityCollections as $cityCollection): ?>
            <tr>
                <td><?= $cityCollection->has('city') ? $this->Html->link($cityCollection->city->name, ['controller' => 'Cities', 'action' => 'view', $cityCollection->city->id]) : '' ?></td>
                <td><?= $cityCollection->has('company') ? $this->Html->link($cityCollection->company->name, ['controller' => 'Companies', 'action' => 'view', $cityCollection->company->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $cityCollection->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $cityCollection->id]) ?>
                    <?= $this->Form->postLink(__('Excluir'), ['action' => 'delete', $cityCollection->id], ['confirm' => __('Tem certeza que deseja excluir a empresa para coleta # {0}?', $cityCollection->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('primeira')) ?>
            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('próxima') . ' >') ?>
            <?= $this->Paginator->last(__('última') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, exibindo {{current}} registro(s) de um total de {{count}} registros.')]) ?></p>
    </div>
</div>
