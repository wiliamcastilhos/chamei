<?php
echo $this->Html->scriptBlock(
	'$("#cnpj").mask("00.000.000/0000-00", {reverse: true});
	
      var optionsCep =  {
        onComplete: function(cep) {
          searchCEPAndCompleteFields();
        },
        onChange: function(cep){
          searchCEPAndCompleteFields();
        },
        placeholder: "_____-___"
      };

      $("#cep").mask("00000-000", optionsCep);
      $("#contact-phone").mask("(00) 0000-0000",{placeholder: "(__) ____-____"});

      //For option input values
      var MaskBehavior = function (val) {
        return val.replace(/\D/g, "").length === 11 ? "(00) 00000-0000" : "(00) 0000-00009";
      },
      Options = {
        onKeyPress: function(val, e, field, options) {
            field.mask(MaskBehavior.apply({}, arguments), options);
          },
          placeholder: "(__) ____-_____"
      };
      $("#cell-phone").mask(MaskBehavior, Options);
      ', ['defer' => true]);
?>