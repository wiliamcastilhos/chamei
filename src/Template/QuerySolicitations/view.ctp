<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\QuerySolicitation $querySolicitation
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Query Solicitation'), ['action' => 'edit', $querySolicitation->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Query Solicitation'), ['action' => 'delete', $querySolicitation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $querySolicitation->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Query Solicitations'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Query Solicitation'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Companies'), ['controller' => 'Companies', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Company'), ['controller' => 'Companies', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Queries'), ['controller' => 'Queries', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Query'), ['controller' => 'Queries', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="querySolicitations view large-9 medium-8 columns content">
    <h3><?= h($querySolicitation->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $querySolicitation->has('user') ? $this->Html->link($querySolicitation->user->name, ['controller' => 'Users', 'action' => 'view', $querySolicitation->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Company') ?></th>
            <td><?= $querySolicitation->has('company') ? $this->Html->link($querySolicitation->company->name, ['controller' => 'Companies', 'action' => 'view', $querySolicitation->company->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Query') ?></th>
            <td><?= $querySolicitation->has('query') ? $this->Html->link($querySolicitation->query->id, ['controller' => 'Queries', 'action' => 'view', $querySolicitation->query->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($querySolicitation->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Normal Value') ?></th>
            <td><?= $this->Number->format($querySolicitation->normal_value) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Express Value') ?></th>
            <td><?= $this->Number->format($querySolicitation->express_value) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($querySolicitation->date) ?></td>
        </tr>
    </table>
</div>
