<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\QuerySolicitation $querySolicitation
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Query Solicitations'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Companies'), ['controller' => 'Companies', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Company'), ['controller' => 'Companies', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Queries'), ['controller' => 'Queries', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Query'), ['controller' => 'Queries', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="querySolicitations form large-9 medium-8 columns content">
    <?= $this->Form->create($querySolicitation) ?>
    <fieldset>
        <legend><?= __('Add Query Solicitation') ?></legend>
        <?php
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('company_id', ['options' => $companies]);
            echo $this->Form->control('query_id', ['options' => $queries]);
            echo $this->Form->control('date');
            echo $this->Form->control('normal_value');
            echo $this->Form->control('express_value');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
