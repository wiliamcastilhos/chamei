<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\QuerySolicitation[]|\Cake\Collection\CollectionInterface $querySolicitations
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Query Solicitation'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Companies'), ['controller' => 'Companies', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Company'), ['controller' => 'Companies', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Queries'), ['controller' => 'Queries', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Query'), ['controller' => 'Queries', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="querySolicitations index large-9 medium-8 columns content">
    <h3><?= __('Query Solicitations') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('company_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('query_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('normal_value') ?></th>
                <th scope="col"><?= $this->Paginator->sort('express_value') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($querySolicitations as $querySolicitation): ?>
            <tr>
                <td><?= $this->Number->format($querySolicitation->id) ?></td>
                <td><?= $querySolicitation->has('user') ? $this->Html->link($querySolicitation->user->name, ['controller' => 'Users', 'action' => 'view', $querySolicitation->user->id]) : '' ?></td>
                <td><?= $querySolicitation->has('company') ? $this->Html->link($querySolicitation->company->name, ['controller' => 'Companies', 'action' => 'view', $querySolicitation->company->id]) : '' ?></td>
                <td><?= $querySolicitation->has('query') ? $this->Html->link($querySolicitation->query->id, ['controller' => 'Queries', 'action' => 'view', $querySolicitation->query->id]) : '' ?></td>
                <td><?= h($querySolicitation->date) ?></td>
                <td><?= $this->Number->format($querySolicitation->normal_value) ?></td>
                <td><?= $this->Number->format($querySolicitation->express_value) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $querySolicitation->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $querySolicitation->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $querySolicitation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $querySolicitation->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
