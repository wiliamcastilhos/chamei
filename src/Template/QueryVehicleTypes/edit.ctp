<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\QueryVehicleType $queryVehicleType
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $queryVehicleType->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $queryVehicleType->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Query Vehicle Types'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Queries'), ['controller' => 'Queries', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Query'), ['controller' => 'Queries', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Vehicle Types'), ['controller' => 'VehicleTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Vehicle Type'), ['controller' => 'VehicleTypes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="queryVehicleTypes form large-9 medium-8 columns content">
    <?= $this->Form->create($queryVehicleType) ?>
    <fieldset>
        <legend><?= __('Edit Query Vehicle Type') ?></legend>
        <?php
            echo $this->Form->control('query_id', ['options' => $queries]);
            echo $this->Form->control('vehicle_type_id', ['options' => $vehicleTypes]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
