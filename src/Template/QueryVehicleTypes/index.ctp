<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\QueryVehicleType[]|\Cake\Collection\CollectionInterface $queryVehicleTypes
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Query Vehicle Type'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Queries'), ['controller' => 'Queries', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Query'), ['controller' => 'Queries', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Vehicle Types'), ['controller' => 'VehicleTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Vehicle Type'), ['controller' => 'VehicleTypes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="queryVehicleTypes index large-9 medium-8 columns content">
    <h3><?= __('Query Vehicle Types') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('query_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('vehicle_type_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($queryVehicleTypes as $queryVehicleType): ?>
            <tr>
                <td><?= $this->Number->format($queryVehicleType->id) ?></td>
                <td><?= $queryVehicleType->has('query') ? $this->Html->link($queryVehicleType->query->id, ['controller' => 'Queries', 'action' => 'view', $queryVehicleType->query->id]) : '' ?></td>
                <td><?= $queryVehicleType->has('vehicle_type') ? $this->Html->link($queryVehicleType->vehicle_type->name, ['controller' => 'VehicleTypes', 'action' => 'view', $queryVehicleType->vehicle_type->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $queryVehicleType->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $queryVehicleType->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $queryVehicleType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $queryVehicleType->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
