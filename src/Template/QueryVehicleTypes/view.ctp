<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\QueryVehicleType $queryVehicleType
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Query Vehicle Type'), ['action' => 'edit', $queryVehicleType->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Query Vehicle Type'), ['action' => 'delete', $queryVehicleType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $queryVehicleType->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Query Vehicle Types'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Query Vehicle Type'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Queries'), ['controller' => 'Queries', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Query'), ['controller' => 'Queries', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Vehicle Types'), ['controller' => 'VehicleTypes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Vehicle Type'), ['controller' => 'VehicleTypes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="queryVehicleTypes view large-9 medium-8 columns content">
    <h3><?= h($queryVehicleType->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Query') ?></th>
            <td><?= $queryVehicleType->has('query') ? $this->Html->link($queryVehicleType->query->id, ['controller' => 'Queries', 'action' => 'view', $queryVehicleType->query->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Vehicle Type') ?></th>
            <td><?= $queryVehicleType->has('vehicle_type') ? $this->Html->link($queryVehicleType->vehicle_type->name, ['controller' => 'VehicleTypes', 'action' => 'view', $queryVehicleType->vehicle_type->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($queryVehicleType->id) ?></td>
        </tr>
    </table>
</div>
