<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Query $query
 */
echo $this->Html->script(['http://maps.googleapis.com/maps/api/js?key=AIzaSyArUKJKMOD2zgR9ojP0d0ynb37OB4O5US0&sensor=false&libraries=places',
    'http://maps.googleapis.com/maps-api-v3/api/js/32/12/common.js',
    'http://maps.googleapis.com/maps-api-v3/api/js/32/12/util.js',
    'http://maps.googleapis.com/maps-api-v3/api/js/32/12/controls.js',
    'http://maps.googleapis.com/maps-api-v3/api/js/32/12/places_impl.js',
    'http://maps.googleapis.com/maps-api-v3/api/js/32/12/stats.js']);
echo $this->Html->css('queries');
?>
<div class="queries form large-9 medium-8 columns content">
    <?php 
    if(isset($budgets)):?>
        <div class="intervals index large-9 medium-8 columns content" id="result" style="display: block;">
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col"><?= __('Entrega Expressa/Urgente') ?></th>
                        <th scope="col"><?= __('Entrega Normal') ?></th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($budgets as $budget): ?>
                    <tr>
                        <td><?= $this->Html->image('/uploads/companies/logo/'.$budget['logo']) ?></td>
                        <td>
                            <?= $this->Number->format($budget['expressValue']) ?>
                            <?php echo $this->Form->hidden('expressValue', ['value'=>$budget['expressValue']]); ?>
                            <br/>
                            <br/>
                            <?= $this->Form->button(__('Solicitar'),[
                                'onclick'=>'budget(this)',
                                'companyid'=>$budget['companyID'],
                                'type'=>'express',
                                'style'=>'font-size: 12px;',
                                'id'=>'express'.$budget['companyID']
                                ]) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($budget['normalValue']) ?>
                            <?php echo $this->Form->hidden('normalValue', ['value'=>$budget['normalValue']]); ?>
                            <br/>
                            <br/>
                            <?= $this->Form->button(__('Solicitar'),[
                                'onclick'=>'budget(this)',
                                'companyid'=>$budget['companyID'],
                                'type'=>'normal',
                                'style'=>'font-size: 12px;',
                                'id'=>'normal'.$budget['companyID'],
                                ]) ?>
                        </td>
                        <td class='status'></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    <?php 
    endif;?>
    <?= $this->Form->create($query) ?>
    <nav class="large-4 medium-4 columns" id="actions-sidebar">
        <fieldset id="collection">
            <legend><?= __('Dados de coleta') ?></legend>
            <?php
                echo $this->Form->hidden('source',['id'=>'source']);
                echo $this->Form->control('collection_cep',['label'=>'Cep','class'=>'cepQuery','type'=>'text','maxlength'=>8,'data-google'=>'postal_code']);
                echo $this->Form->control('collection_ample_address',
                    [
                        'label'=>'Local (Texto livre)',
                        'class'=>'ampleAddressQuery',
                        'onFocus'=>'geolocate()',
                        'value' => 'Rua Adélino Andreata Samorro - Santa Catarina, Caxias do Sul - RS, Brazil' 
                    ]
                );
                echo $this->Form->control('collection_address',['label'=>'Rua','class'=>'addressQuery','data-google'=>'route']);
                echo $this->Form->control('collection_street_number',['label'=>'Nº','class'=>'streetNumberQuery','data-google'=>'street_number','value'=>'554']);
                echo $this->Form->control('collection_complement',['label'=>'Complemento','class'=>'complementQuery']);
                echo $this->Form->input(
                    'collection_states_id', 
                    [
                        'type' => 'select',
                        'multiple' => false,
                        'options' => $ufStates,
                        'empty' => 'Selecione a UF',
                        'onchange' =>'loadCitiesQuery("collection")',
                        'label' => 'UF',
                        'required' => true,
                        'class' => 'state_id_query'
                    ]
                );
                echo '<div class="loading"><b>' . $this->Html->image('load.gif', ['alt' => 'Aguarde, carregando cidades...']) . ' Aguarde, carregando cidades...</b></div>';
                echo $this->Form->control('city_collection_id', ['empty'=>'Selecione a cidade','label'=>'Cidade','class' => 'city_id_query']);
                echo $this->Form->control('collection_neighborhood',['label'=>'Bairro','class'=>'neighborhoodQuery','data-google'=>'sublocality_level_1']);
                echo $this->Form->control('collection_references',['label'=>'Referência','class'=>'referencesQuery']);
                echo $this->Form->hidden('cityNameSelected', ['direction'=>'collection']);
            ?>
        </fieldset>
    </nav>
    <nav class="large-4 medium-4 columns" id="actions-sidebar">
        <fieldset id="delivery">
            <legend><?= __('Dados de entrega') ?></legend>
            <?php
                echo $this->Form->hidden('destination',['id'=>'destination']);
                echo $this->Form->control('delivery_cep',['label'=>'Cep','class'=>'cepQuery','type'=>'text','maxlength'=>8,'data-google'=>'postal_code']);
                echo $this->Form->control('delivery_ample_address',
                    [
                        'label'=>'Local (Texto livre)',
                        'onfocus' => 'geolocate()',
                        'class'=>'ampleAddressQuery',
                        'value'=> 'Rua Vergínio Tonieto - Kayser, Caxias do Sul - RS, Brazil'
                    ]
                );
                echo $this->Form->control('delivery_address',['label'=>'Rua','class'=>'addressQuery','data-google'=>'route']);
                echo $this->Form->control('delivery_street_number',['label'=>'Nº','class'=>'streetNumberQuery','data-google'=>'street_number','value'=>2137]);
                echo $this->Form->control('delivery_complement',['label'=>'Complemento','class'=>'complementQuery']);
                echo $this->Form->input(
                    'delivery_states_id', 
                    [
                        'type' => 'select',
                        'multiple' => false,
                        'options' => $ufStates,
                        'empty' => 'Selecione a UF',
                        'onchange' =>'loadCitiesQuery("delivery")',
                        'label' => 'UF',
                        'required' => true,
                        'class' => 'state_id_query'
                    ]
                );
                echo '<div class="loading"><b>' . $this->Html->image('load.gif', ['alt' => 'Aguarde, carregando cidades...']) . ' Aguarde, carregando cidades...</b></div>';
                echo $this->Form->control('city_delivery_id', ['empty'=>'Selecione a cidade','label'=>'Cidade','class' => 'city_id_query']);
                echo $this->Form->control('delivery_neighborhood',['label'=>'Bairro','class'=>'neighborhoodQuery','data-google'=>'sublocality_level_1']);
                echo $this->Form->control('delivery_references',['label'=>'Referência','class'=>'referencesQuery']);
                echo $this->Form->hidden('cityNameSelected', ['direction'=>'delivery']);
            ?>
        </fieldset>
    </nav>
    <fieldset id="delivery">
        <legend><?= __('Tipo de veículo') ?></legend>
        <?php
        // echo $this->Form->input('distancia',['type'=>'text','label'=>'Distância','style'=>'width: inherit']);
        echo $this->Form->input('vehicleTypes.id', [
            'label' => false,
            'type' => 'select',
            'multiple' => 'checkbox',
            'options' => $vehicleTypes,
            'hiddenField' => false
        ]);

    // $fields = array();
    // foreach($vehicleTypes as $vehicleTypeId=>$vehicleTypeName){
    //     $fields["vehicleTypes[id]"] =
    //         [   'type' => 'checkbox', 
    //             'empty' => true, 
    //             'value' => $vehicleTypeId, 
    //             'label'=> $vehicleTypeName
    //         ];
    // }
    // echo $this->Form->controls($fields, ['legend' => 'Tipo de veículo','multiple'=> 'checkbox']);
    ?>
    </fieldset>
    <?php echo $this->Form->hidden('distance',['id'=>'distance']);?>
    <?= $this->Form->button(__('Buscar cotações'),['onclick'=>'completeAddress()']) ?>
    <?= $this->Form->end() ?>
    <div class="container mostradados">
        <div class="row" >
          <div class='col-md-12' id='maplocation' style="height: 250px;" ></div>
        </div>
        <div id="panellocation" ></div>
        <div id="distancias"></div>
    </div>
</div>
    <?php 

    echo $this->Html->scriptBlock('

    $(".cepQuery").blur(function(){
        requestAdrressQuery($(this).closest("fieldset").attr("id"));
    });
    $(".cepQuery").mask("00000-000", {placeholder: "_____-___"});

    function requestAdrressQuery(fieldsetId){
        var cep = $("#"+fieldsetId+" .cepQuery").val().replace(/[.-]+/g,"");
        if(cep != undefined && cep != ""){
            $.ajax({
                type: "POST",
                url: "http://viacep.com.br/ws/"+cep+"/json/",
                success: function(result) {
                    loadAdrressQuery(fieldsetId,result);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            });
        }
    }

    function loadAdrressQuery(idFieldset,json){
        var address = json.logradouro;   
        var complement = json.complemento;
        var neighborhood = json.bairro;
        var nmCity = json.localidade;
        var nmUf = json.uf;
        var requestUrl = "/myapp/cities/findStateAndCity";
        var dataSet = { displayUf: true , nmUf:nmUf, nmCity: nmCity};

        $.ajax({
            type: "POST",
            url: requestUrl,
            data: dataSet,
            success: function(result) {
                
                $("#"+idFieldset+" .loading").fadeIn(50);
                $("#"+idFieldset+" .state_id_query option").each(function(i,elemen){
                    if(elemen.text == nmUf){
                        $(elemen).attr("selected","selected");
                    }
                });
                $("#"+idFieldset+" .city_id_query").html(result.cities);
                $("#"+idFieldset+" .loading").fadeOut(50);
                $("#"+idFieldset+" .addressQuery").val(address);
                $("#"+idFieldset+" .neighborhoodQuery").val(neighborhood);
                loadDistance();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }

    function loadCitiesQuery(direction){
        $("#"+direction+" .loading").toggle(50);
        var dataSet = {stateId: $("#"+direction+" .state_id_query").val(), cityDirection:direction};
        var requestUrl = "/myapp/cities/list";
        $.ajax({
            type: "POST",
            url: requestUrl,
            data: dataSet,
            success: function(result) {
                $("#"+direction+" .city_id_query").html(result);
                selectCityQuery(direction);
                $("#"+direction+" .loading").toggle(50);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }

    function selectCityQuery(direction){
        var cityNameSelect = $("#"+direction+" input[name=cityNameSelected]").val();
        if(cityNameSelect != "" 
            && cityNameSelect != undefined){
            $("#"+direction+" .city_id_query option").each(function() {
                if(this.text == cityNameSelect){
                    $(this).attr("selected","selected");
                    $("#"+direction+" .city_id_query").trigger("change");
                }
            });
        }
    }

    function budget(component){
        var dataSet = {
            type: $(component).attr("type"), 
            companyID:$(component).attr("companyID"),
            normalValue:$("input[name=normalValue]").val(),
            expressValue:$("input[name=expressValue]").val(),
            queryId:$("input[name=queryId]").val(),
        };
        var requestUrl = "/myapp/query-solicitations/budget";
        var buttonId = $("#"+$(component).attr("type")+$(component).attr("companyID"));
        $.ajax({
            type: "POST",
            url: requestUrl,
            data: dataSet,
            success: function(result) {
                if(result.status == "OK"){
                    var img = $(\'<img id="imgStatus">\');
                    img.attr("src", "/img/wait.jpg");
                    img.attr("width", "50px");
                    img.appendTo("#result  .status");
                    $(buttonId).fadeOut(50);    
                }else{
                    alert("Error");
                }
                
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });   
    }

    ///////////////DISTANCE/////////////////////
    function loadDistance(){
        
        /*if($("#collection .cepQuery").val()!= "" && $("#delivery .cepQuery").val()!= ""){
            var address1 = $("#collection .addressQuery").val();
            var street_number1 = $("#collection .streetNumberQuery").val();
            var neighborhood1 = $("#collection .neighborhoodQuery").val();
            var city1 = $("#collection .city_id_query option:selected").text();
            var state1 = $("#collection .state_id_query option:selected").text();
            var cep1 = $("#collection .cepQuery").val();

            var address2 = $("#delivery .addressQuery").val();
            var street_number2 = $("#delivery .streetNumberQuery").val();
            var neighborhood2 = $("#delivery .neighborhoodQuery").val();
            var city2 = $("#delivery .city_id_query option:selected").text();
            var state2 = $("#delivery .state_id_query option:selected").text();
            var cep2 = $("#delivery .cepQuery").val();

            params =    "units:metric"+ 
                        "&origins:"+encodeURI(address1+", "+street_number1+" Bairro "+neighborhood1+
                        ". "+city1+"-"+state1)+ 
                        "&destinations:"+encodeURI(address2+", "+street_number2+" Bairro "+neighborhood2+
                        ". "+city2+"-"+state2)+ 
                        "&key:AIzaSyArUKJKMOD2zgR9ojP0d0ynb37OB4O5US0";

            var requestUrl = "https://maps.googleapis.com/maps/api/distancematrix/json?"+params;
            $.get( requestUrl, function( data ) {
                if(data.status == "OK"){
                    $( "#distancia" ).val(data.distance.text);
                }else{
                    console.log(data);
                }
            });
        }*/


    }
    ////////////////////////////////////////////
    ///////////////MAPS/////////////////////////
    function completeAddress(){
        var sourceValue = "";
        var destinationValue = "";
        if($("#collection .addressQuery").val() != ""){
            sourceValue += $("#collection .addressQuery").val();
        }
        if($("#collection .streetNumberQuery").val() != ""){
            sourceValue += ", "+$("#collection .streetNumberQuery").val();
        }
        if($("#collection .neighborhoodQuery").val() != ""){
            sourceValue += " "+$("#collection .neighborhoodQuery").val();
        }
        if($("#collection .city_id_query :selected").text() != ""){
            sourceValue += " "+$("#collection .city_id_query :selected").text()
        }
        if($("#collection .state_id_query :selected").text() != ""){
            sourceValue += " - "+$("#collection .state_id_query :selected").text()
        }
        if($("#delivery .addressQuery").val() != ""){
            destinationValue += $("#delivery .addressQuery").val();
        }
        if($("#delivery .streetNumberQuery").val() != ""){
            destinationValue += ", "+$("#collection .streetNumberQuery").val();
        }
        if($("#delivery .neighborhoodQuery").val() != ""){
            destinationValue += " "+$("#delivery .neighborhoodQuery").val();
        }
        if($("#delivery .city_id_query :selected").text() != ""){
            destinationValue += " "+$("#delivery .city_id_query :selected").text()
        }
        if($("#delivery .state_id_query :selected").text() != ""){
            destinationValue += " - "+$("#delivery .state_id_query :selected").text()
        }
        $("#source").val(sourceValue);
        $("#destination").val(destinationValue);
        $("html,body").animate({ scrollTop: 300 }, "slow");
        $("#maplocation").css("display", "block");
        //get_rout();
        $("#distance").val("12000");
    }
    var source, destination;
    var direction = new google.maps.DirectionsRenderer;
    var directionsService = new google.maps.DirectionsService;
    google.maps.event.addDomListener(window, "load", function () {
            new google.maps.places.SearchBox($("#source"));
            new google.maps.places.SearchBox($("#destination"));
        });
    function get_rout() {
        var mapOptions = {
            mapTypeControl: false,
            zoom:13
        };
        map = new google.maps.Map(document.getElementById("maplocation"), mapOptions);
        direction.setMap(map);
        direction.setPanel(document.getElementById("panellocation"));
        source = document.getElementById("source").value;
        destination = document.getElementById("destination").value;

        var request = {
            origin: source,
            destination: destination,
            travelMode: google.maps.TravelMode.DRIVING
        };

        directionsService.route(request, function (response, status) {
            debugger;
            if (status == google.maps.DirectionsStatus.OK) {
                direction.setDirections(response);
                $("#maplocation").css("display", "block");
            }else{
                $("#maplocation").css("display", "none");
                alert("Endereço incorreto");
            }
        });

        var service = new google.maps.DistanceMatrixService();
        debugger;
        service.getDistanceMatrix({
            origins: [source],
            destinations: [destination],
            travelMode: google.maps.TravelMode.DRIVING,
            unitSystem: google.maps.UnitSystem.METRIC,
            avoidHighways: false,
            avoidTolls: false
        }, function (response, status) {
                debugger;
            if (status == google.maps.DistanceMatrixStatus.OK && response.rows[0].elements[0].status != "ZERO_RESULTS") {
                var distance = response.rows[0].elements[0].distance.text;
                var duration = response.rows[0].elements[0].duration.text;

                distancefinel = distance.split(" ");
                alert("Final "+distancefinel);
                $(".distancia").val(distancefinel[0]);

                // endColeta = document.getElementById("endColeta").value;
                // endEntrega = document.getElementById("endEntrega").value;
                // numeroColeta = document.getElementById("numeroColeta").value;
                // numeroEntrega = document.getElementById("numeroEntrega").value;
                // bairroColeta = document.getElementById("bairroColeta").value;
                // bairroEntrega = document.getElementById("bairroEntrega").value;
                // cidadeColeta = document.getElementById("cidadeColeta").value;
                // cidadeEntrega = document.getElementById("cidadeEntrega").value;
                // estadoColeta = document.getElementById("estadoColeta").value;
                // estadoEntrega = document.getElementById("estadoEntrega").value;
                // dimensoes = document.getElementById("dimensoes").value;
        }else{
            alert("Não é possível encontrar a distância pela estrada.");
        }
    });
}

var placeSearchCollection, placeSearchDelivery, autocompleteCollection, autocompleteDelivery;
var componentForm = {
    street_number: "short_name",
    route: "long_name",
    administrative_area_level_2: "long_name",
    administrative_area_level_1: "short_name",
    sublocality_level_1: "long_name"
};
function initAutocomplete() {
    autocompleteCollection = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById("collection-ample-address")),
                        {types: ["geocode"]});

    autocompleteCollection.addListener("place_changed", fillInAddressCollection);

    autocompleteDelivery = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById("delivery-ample-address")),
                        {types: ["geocode"]});

    autocompleteDelivery.addListener("place_changed", fillInAddressDelivery);
    $(autocompleteDelivery).attr("direction","delivery");
}
function fillInAddressCollection() {
    var placeCollection = autocompleteCollection.getPlace();
    for (var component in componentForm) {
        $("#collection input[data-google="+component+"]").val("");
        $("#collection input[data-google="+component+"]").prop("disabled", false);
    }
    var cityNameSelect = "";

    for (var i = 0; i < placeCollection.address_components.length; i++) {
        var addressType = placeCollection.address_components[i].types[0];
        if (componentForm[addressType]) {
            var val = placeCollection.address_components[i][componentForm[addressType]];
            if(addressType === "administrative_area_level_2"){
                cityNameSelect = val;
            } else if(addressType === "administrative_area_level_1"){
                $("#collection .state_id_query option").each(function(i,elemen){
                    if(elemen.text == val){
                        $(elemen).attr("selected","selected");
                        $(this).trigger("change");
                        $("input[name=cityNameSelected][direction=collection]").val(cityNameSelect);
                    }
                });
            } else {
                $("#collection input[data-google="+addressType+"]").val(val);
            }
        }
    }
}
function fillInAddressDelivery() {
    var placeDelivery = autocompleteDelivery.getPlace();
    for (var component in componentForm) {
        $("#delivery input[data-google="+component+"]").val("");
        $("#delivery input[data-google="+component+"]").prop("disabled", false);
    }
    var cityNameSelect = "";

    for (var i = 0; i < placeDelivery.address_components.length; i++) {
        var addressType = placeDelivery.address_components[i].types[0];
        console.log(placeDelivery.address_components[i]);
        if (componentForm[addressType]) {
            var val = placeDelivery.address_components[i][componentForm[addressType]];
            if(addressType === "administrative_area_level_2"){
                cityNameSelect = val;
            } else if(addressType === "administrative_area_level_1"){
                $("#delivery .state_id_query option").each(function(i,elemen){
                    if(elemen.text == val){
                        $(elemen).attr("selected","selected");
                        $(this).trigger("change");
                        $("input[name=cityNameSelected][direction=delivery]").val(cityNameSelect);
                    }
                });
            } else {
                $("#delivery input[data-google="+addressType+"]").val(val);
            }
        }
    }
}

function geolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            debugger;
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            autocompleteCollection.setBounds(circle.getBounds());
            autocompleteDelivery.setBounds(circle.getBounds());
        }, handle_error);
    }
}

function handle_error(error){
    console.log(error);
}

initAutocomplete();

function showResult(){
    $("#result").fadeIn(
        function(){
            $("html,body").animate({ scrollTop: 300 }, "slow");
        }
    )
}


    ////////////////////////////////////////////

    ', ['defer' => false]);
?>