<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Query $query
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $query->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $query->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Queries'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List City Collections'), ['controller' => 'CityCollections', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New City Collection'), ['controller' => 'CityCollections', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List City Deliveries'), ['controller' => 'CityDeliveries', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New City Delivery'), ['controller' => 'CityDeliveries', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Query Solicitations'), ['controller' => 'QuerySolicitations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Query Solicitation'), ['controller' => 'QuerySolicitations', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="queries form large-9 medium-8 columns content">
    <?= $this->Form->create($query) ?>
    <fieldset>
        <legend><?= __('Edit Query') ?></legend>
        <?php
            echo $this->Form->control('city_collection_id', ['options' => $cityCollections]);
            echo $this->Form->control('city_delivery_id', ['options' => $cityDeliveries]);
            echo $this->Form->control('date');
            echo $this->Form->control('normal_value');
            echo $this->Form->control('express_value');
            echo $this->Form->control('collection_address');
            echo $this->Form->control('collection_street_number');
            echo $this->Form->control('collection_neighborhood');
            echo $this->Form->control('collection_cep');
            echo $this->Form->control('collection_complement');
            echo $this->Form->control('collection_references');
            echo $this->Form->control('delivery_address');
            echo $this->Form->control('delivery_street_number');
            echo $this->Form->control('delivery_neighborhood');
            echo $this->Form->control('delivery_cep');
            echo $this->Form->control('delivery_complement');
            echo $this->Form->control('delivery_references');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
