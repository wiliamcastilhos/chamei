<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Query $query
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Query'), ['action' => 'edit', $query->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Query'), ['action' => 'delete', $query->id], ['confirm' => __('Are you sure you want to delete # {0}?', $query->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Queries'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Query'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List City Collections'), ['controller' => 'CityCollections', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New City Collection'), ['controller' => 'CityCollections', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List City Deliveries'), ['controller' => 'CityDeliveries', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New City Delivery'), ['controller' => 'CityDeliveries', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Query Solicitations'), ['controller' => 'QuerySolicitations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Query Solicitation'), ['controller' => 'QuerySolicitations', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="queries view large-9 medium-8 columns content">
    <h3><?= h($query->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('City Collection') ?></th>
            <td><?= $query->has('city_collection') ? $this->Html->link($query->city_collection->id, ['controller' => 'CityCollections', 'action' => 'view', $query->city_collection->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('City Delivery') ?></th>
            <td><?= $query->has('city_delivery') ? $this->Html->link($query->city_delivery->id, ['controller' => 'CityDeliveries', 'action' => 'view', $query->city_delivery->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Collection Address') ?></th>
            <td><?= h($query->collection_address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Collection Street Number') ?></th>
            <td><?= h($query->collection_street_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Collection Neighborhood') ?></th>
            <td><?= h($query->collection_neighborhood) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Collection Complement') ?></th>
            <td><?= h($query->collection_complement) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Collection References') ?></th>
            <td><?= h($query->collection_references) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Delivery Address') ?></th>
            <td><?= h($query->delivery_address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Delivery Street Number') ?></th>
            <td><?= h($query->delivery_street_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Delivery Neighborhood') ?></th>
            <td><?= h($query->delivery_neighborhood) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Delivery Complement') ?></th>
            <td><?= h($query->delivery_complement) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Delivery References') ?></th>
            <td><?= h($query->delivery_references) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($query->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Normal Value') ?></th>
            <td><?= $this->Number->format($query->normal_value) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Express Value') ?></th>
            <td><?= $this->Number->format($query->express_value) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Collection Cep') ?></th>
            <td><?= $this->Number->format($query->collection_cep) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Delivery Cep') ?></th>
            <td><?= $this->Number->format($query->delivery_cep) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($query->date) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Query Solicitations') ?></h4>
        <?php if (!empty($query->query_solicitations)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Company Id') ?></th>
                <th scope="col"><?= __('Query Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($query->query_solicitations as $querySolicitations): ?>
            <tr>
                <td><?= h($querySolicitations->id) ?></td>
                <td><?= h($querySolicitations->user_id) ?></td>
                <td><?= h($querySolicitations->company_id) ?></td>
                <td><?= h($querySolicitations->query_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'QuerySolicitations', 'action' => 'view', $querySolicitations->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'QuerySolicitations', 'action' => 'edit', $querySolicitations->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'QuerySolicitations', 'action' => 'delete', $querySolicitations->id], ['confirm' => __('Are you sure you want to delete # {0}?', $querySolicitations->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
