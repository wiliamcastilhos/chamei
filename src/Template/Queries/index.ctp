<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Query[]|\Cake\Collection\CollectionInterface $queries
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Query'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List City Collections'), ['controller' => 'CityCollections', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New City Collection'), ['controller' => 'CityCollections', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List City Deliveries'), ['controller' => 'CityDeliveries', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New City Delivery'), ['controller' => 'CityDeliveries', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Query Solicitations'), ['controller' => 'QuerySolicitations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Query Solicitation'), ['controller' => 'QuerySolicitations', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="queries index large-9 medium-8 columns content">
    <h3><?= __('Queries') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('city_collection_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('city_delivery_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('normal_value') ?></th>
                <th scope="col"><?= $this->Paginator->sort('express_value') ?></th>
                <th scope="col"><?= $this->Paginator->sort('collection_address') ?></th>
                <th scope="col"><?= $this->Paginator->sort('collection_street_number') ?></th>
                <th scope="col"><?= $this->Paginator->sort('collection_neighborhood') ?></th>
                <th scope="col"><?= $this->Paginator->sort('collection_cep') ?></th>
                <th scope="col"><?= $this->Paginator->sort('collection_complement') ?></th>
                <th scope="col"><?= $this->Paginator->sort('collection_references') ?></th>
                <th scope="col"><?= $this->Paginator->sort('delivery_address') ?></th>
                <th scope="col"><?= $this->Paginator->sort('delivery_street_number') ?></th>
                <th scope="col"><?= $this->Paginator->sort('delivery_neighborhood') ?></th>
                <th scope="col"><?= $this->Paginator->sort('delivery_cep') ?></th>
                <th scope="col"><?= $this->Paginator->sort('delivery_complement') ?></th>
                <th scope="col"><?= $this->Paginator->sort('delivery_references') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($queries as $query): ?>
            <tr>
                <td><?= $this->Number->format($query->id) ?></td>
                <td><?= $query->has('city_collection') ? $this->Html->link($query->city_collection->id, ['controller' => 'CityCollections', 'action' => 'view', $query->city_collection->id]) : '' ?></td>
                <td><?= $query->has('city_delivery') ? $this->Html->link($query->city_delivery->id, ['controller' => 'CityDeliveries', 'action' => 'view', $query->city_delivery->id]) : '' ?></td>
                <td><?= h($query->date) ?></td>
                <td><?= $this->Number->format($query->normal_value) ?></td>
                <td><?= $this->Number->format($query->express_value) ?></td>
                <td><?= h($query->collection_address) ?></td>
                <td><?= h($query->collection_street_number) ?></td>
                <td><?= h($query->collection_neighborhood) ?></td>
                <td><?= $this->Number->format($query->collection_cep) ?></td>
                <td><?= h($query->collection_complement) ?></td>
                <td><?= h($query->collection_references) ?></td>
                <td><?= h($query->delivery_address) ?></td>
                <td><?= h($query->delivery_street_number) ?></td>
                <td><?= h($query->delivery_neighborhood) ?></td>
                <td><?= $this->Number->format($query->delivery_cep) ?></td>
                <td><?= h($query->delivery_complement) ?></td>
                <td><?= h($query->delivery_references) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $query->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $query->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $query->id], ['confirm' => __('Are you sure you want to delete # {0}?', $query->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
