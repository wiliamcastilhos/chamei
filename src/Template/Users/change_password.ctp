<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Listar usuários'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Listar tipos de usuários'), ['controller' => 'Roles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Novo tipo de usuário'), ['controller' => 'Roles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Listar cidades'), ['controller' => 'Cities', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nova cidade'), ['controller' => 'Cities', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create() ?>
        <fieldset>
            <legend><?= __('Por favor entre a nova senha.') ?></legend>
            <?= $this->Form->control('current_password',['label'=>'Senha atual','type' => 'password','required'=>true]);?>
            <?= $this->Form->control('password1',['label'=>'Nova senha','type' => 'password','required'=>true]);?>
            <?= $this->Form->control('password2',['label'=>'Confirmar senha','type'=>'password','required'=>true]); ?>
        </fieldset>
        <?= $this->Form->button(__('Atualiza senha')); ?>
    <?= $this->Form->end() ?>
</div>