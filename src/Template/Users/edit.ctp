<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Form->postLink(
                __('Excluir'),
                ['action' => 'delete', $user->id],
                ['confirm' => __('Tem certeza que deseja excluir o usuário # {0}?', $user->name)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Listar usuários'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Listar tipos de usuários'), ['controller' => 'Roles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Novo tipo de usuário'), ['controller' => 'Roles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Listar cidades'), ['controller' => 'Cities', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nova cidade'), ['controller' => 'Cities', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Editar usuário') ?></legend>
        <?php
            echo $this->Form->control('roles_id', ['options' => $roles,'empty'=>'Selecione o tipo de usuário']);
            echo $this->Form->control('name',['label'=>'Nome']);
            echo $this->Form->control('email',['label'=>'E-mail','type' => 'email']);
            echo $this->Form->control('cep');
            echo $this->Form->control('address',['label'=>'Endereço']);
            echo $this->Form->control('street_number',['label'=>'Nº']);
            echo $this->Form->control('complement',['label'=>'Complemento']);
            echo $this->Form->control('neighborhood',['label'=>'Bairro']);
            echo $this->Form->input(
                'states_id', 
                [
                    'type' => 'select',
                    'multiple' => false,
                    'options' => $states, 
                    'empty' => 'Selecione um estado',
                    'onchange' =>'loadCities()',
                    'label' => 'Estado',
                    'required' => true
                ]
            );
            echo '<div class="loading"><b>' . $this->Html->image('load.gif', ['alt' => 'Aguarde, carregando cidades...']) . ' Aguarde, carregando cidades...</b></div>';
            echo $this->Form->control('city_id', ['label' => 'Cidade','empty'=> 'Selecione uma cidade']);
            echo $this->Form->control('contact_phone',['label'=>'Telefone Contato']);
            echo $this->Form->control('cell_phone',['label'=>'Telefone Celular']);
            echo $this->Form->control('is_active',['label'=>'Ativo']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Salvar')) ?>
    <?= $this->Form->end() ?>
    <?= $this->Form->hidden('stateIdSelected',['value'=>$stateIdSelected]);?>
    <?= $this->Form->hidden('cityIdSelected',['value'=>$cityIdSelected]);?>
</div>