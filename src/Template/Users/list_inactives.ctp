<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Novo usuário público'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Novo usuário'), ['action' => 'add_admin']) ?></li>
        <li><?= $this->Html->link(__('Listar tipos de usuário'), ['controller' => 'Roles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Novo tipo de usuário'), ['controller' => 'Roles', 'action' => 'add']) ?></li>
        <li><?= $this->Form->postLink(__('Alterar senha'), ['action' => 'changePassword', $currentUser['id']]) ?>
        <li><?= $this->Html->link(__('Listar usuários inativos'), ['action' => 'listInactives']) ?></li>
        <li><?= $this->Html->link(__('Listar cidades'), ['controller' => 'Cities', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nova cidade'), ['controller' => 'Cities', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create() ?>
    <fieldset>
        <legend><?= __('Usuários inativos') ?></legend>
        <?php   foreach ($users as $user):
                    echo $this->Form->checkbox('is_active[]',['label'=>'Ativo','value'=>$user->id]);
                    echo $this->Html->link(__($user->name), ['action' => 'view', $user->id]);
                    echo '<br /><br />';
                endforeach; ?>
    </fieldset>
    <?= $this->Form->button(__('Salvar')) ?>
    <?= $this->Form->end() ?>
</div>