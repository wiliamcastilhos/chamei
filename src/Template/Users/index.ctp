<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Novo usuário público'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Novo usuário'), ['action' => 'add_admin']) ?></li>
        <li><?= $this->Html->link(__('Listar tipos de usuário'), ['controller' => 'Roles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Novo tipo de usuário'), ['controller' => 'Roles', 'action' => 'add']) ?></li>
        <li><?= $this->Form->postLink(__('Alterar senha'), ['action' => 'changePassword', $currentUser['id']]) ?>
        <li><?= $this->Html->link(__('Listar usuários inativos'), ['action' => 'listInactives']) ?></li>
        <li><?= $this->Html->link(__('Listar cidades'), ['controller' => 'Cities', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nova cidade'), ['controller' => 'Cities', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="users index large-9 medium-8 columns content">
    <h3><?= __('Usuários') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('roles_id',['label'=>'Tipo de usuário']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('name',['label'=>'Nome']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('email',['label'=>'E-mail']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('address',['label'=>'Endereço']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('neighborhood',['label'=>'Bairro']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('city_id',['label'=>'Cidade']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('cell_phone',['label'=>'Telefone']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_active',['label'=>'Ativo']) ?></th>
                <th scope="col" class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
            <tr>
                <td><?= $user->has('role') ? $this->Html->link($user->role->role, ['controller' => 'Roles', 'action' => 'view', $user->role->id]) : '' ?></td>
                <td><?= h($user->name) ?></td>
                <td><?= h($user->email) ?></td>
                <td><?= h($user->address) ?></td>
                <td><?= h($user->neighborhood) ?></td>
                <td><?= $user->has('city') ? $this->Html->link($user->city->name, ['controller' => 'Cities', 'action' => 'view', $user->city->id]) : '' ?></td>
                <td><?= h($user->cell_phone) ?></td>
                <td><?= h($user->is_active==0?"Não":"Sim") ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $user->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $user->id]) ?>
                    <?= $this->Form->postLink(__('Excluir'), ['action' => 'delete', $user->id], ['confirm' => __('Tem certeza que deseja excluir # {0}?', $user->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('primeira')) ?>
            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('próxima') . ' >') ?>
            <?= $this->Paginator->last(__('última') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, exibindo {{current}} registro(s) de um total de {{count}}')]) ?></p>
    </div>
</div>
