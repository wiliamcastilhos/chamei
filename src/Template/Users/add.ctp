<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Login'), ['action' => 'login']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Adicionar usuário') ?></legend>
        <?php
            echo $this->Form->hidden('roles_id', ['value'=>$publicRole]);
            echo $this->Form->control('name',['label'=>'Nome']);
            echo $this->Form->control('email',['label'=>'E-mail','type' => 'email','error' => ['The provided value is invalid' => __('Formato de e-mail inválido')]]);
            echo $this->Form->control('cep');
            echo $this->Form->control('address',['label'=>'Endereço']);
            echo $this->Form->control('street_number',['label'=>'Nº']);
            echo $this->Form->control('complement',['label'=>'Complemento']);
            echo $this->Form->control('neighborhood',['label'=>'Bairro']);
            echo $this->Form->input(
                'states_id', 
                [
                    'type' => 'select',
                    'multiple' => false,
                    'options' => $states, 
                    'empty' => 'Selecione um estado',
                    'onchange' =>'loadCities()',
                    'label' => 'Estado',
                    'required' => true
                ]
            );
            echo '<div class="loading"><b>' . $this->Html->image('load.gif', ['alt' => 'Aguarde, carregando cidades...']) . ' Aguarde, carregando cidades...</b></div>';
            echo $this->Form->control('city_id', ['label' => 'Cidade','empty'=> 'Selecione uma cidade']);
            echo $this->Form->control('contact_phone',['label'=>'Telefone Contato']);
            echo $this->Form->control('cell_phone',['label'=>'Telefone Celular']);
            echo $this->Form->control('password',['label'=>'Senha']);
            echo $this->Form->control('confirm_password',['label'=>'Confirmar senha','type'=>'password']);
            echo $this->Form->hidden('is_active',['value'=>0]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Salvar')) ?>
    <?= $this->Form->end() ?>
    <?= $this->Form->hidden('cityIdSelected',['value'=>$cityIdSelected  ])?>
</div>