<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\City $city
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Listar cidades'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Listar estados'), ['controller' => 'States', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Novo estado'), ['controller' => 'States', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Listar empresas'), ['controller' => 'Companies', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nova empresa'), ['controller' => 'Companies', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="cities form large-9 medium-8 columns content">
    <?= $this->Form->create($city) ?>
    <fieldset>
        <legend><?= __('Adicionar cidade') ?></legend>
        <?php
            echo $this->Form->control('name',['label'=>'Nome']);
            echo $this->Form->control('state_id', ['options' => $states,'label'=>'Estado']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Salvar')) ?>
    <?= $this->Form->end() ?>
</div>
