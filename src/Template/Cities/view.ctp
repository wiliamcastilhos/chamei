<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\City $city
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Editar cidade'), ['action' => 'edit', $city->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Excluir cidade'), ['action' => 'delete', $city->id], ['confirm' => __('Tem certeza que deseja excluir a cidade # {0}?', $city->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar cidades'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nova cidade'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Listar estados'), ['controller' => 'States', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Novo estado'), ['controller' => 'States', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Listar Empresas'), ['controller' => 'Companies', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nova empresa'), ['controller' => 'Companies', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="cities view large-9 medium-8 columns content">
    <h3><?= h($city->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nome') ?></th>
            <td><?= h($city->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Estado') ?></th>
            <td><?= $city->has('state') ? $this->Html->link($city->state->name, ['controller' => 'States', 'action' => 'view', $city->state->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($city->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Empresas relacionadas') ?></h4>
        <?php if (!empty($city->companies)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Nome') ?></th>
                <th scope="col"><?= __('E-mail') ?></th>
                <th scope="col"><?= __('Endereço') ?></th>
                <th scope="col"><?= __('Bairro') ?></th>
                <th scope="col"><?= __('Telefone') ?></th>
                <th scope="col"><?= __('Ativo') ?></th>
                <th scope="col" class="actions"><?= __('Ações') ?></th>
            </tr>
            <?php foreach ($city->companies as $companies): ?>
            <tr>
                <td><?= h($companies->name) ?></td>
                <td><?= h($companies->email) ?></td>
                <td><?= h($companies->address) ?></td>
                <td><?= h($companies->neighborhood) ?></td>
                <td><?= h($companies->cell_phone) ?></td>
                <td><?= h($companies->is_active==0?'Não':'Sim') ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Visualizar'), ['controller' => 'Companies', 'action' => 'view', $companies->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['controller' => 'Companies', 'action' => 'edit', $companies->id]) ?>
                    <?= $this->Form->postLink(__('Excluir'), ['controller' => 'Companies', 'action' => 'delete', $companies->id], ['confirm' => __('Tem certeza que deseja excluir a empresa # {0}?', $companies->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
