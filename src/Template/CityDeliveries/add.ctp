<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CityDelivery $cityDelivery
 */
if(isset($company)){
    $titlePage = 'Adicionar cidade de entrega para a empresa '.$company->name;
}else{
    $titlePage = 'Adicionar cidade para entrega';
}
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Listar cidades para entrega'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Listar cidades'), ['controller' => 'Cities', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nova cidade'), ['controller' => 'Cities', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Listar empresas'), ['controller' => 'Companies', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nova empresa'), ['controller' => 'Companies', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="cityDeliveries form large-9 medium-8 columns content">
    <?= $this->Form->create($cityDelivery) ?>
    <fieldset>
        <legend><?= __($titlePage) ?></legend>
        <?php
            echo $this->Form->input(
                    'states_id', 
                    [
                        'type' => 'select',
                        'multiple' => false,
                        'options' => $states, 
                        'empty' => 'Selecione um estado',
                        'onchange' =>'loadCities()',
                        'label' => 'Estado',
                        'required' => true
                    ]
                );
            echo '<div class="loading"><b>' . $this->Html->image('load.gif', ['alt' => 'Aguarde, carregando cidades...']) . ' Aguarde, carregando cidades...</b></div>';
            echo $this->Form->control('city_id', ['label' => 'Cidade','empty'=> 'Selecione uma cidade']);
            if(isset($company)){
                echo $this->Form->hidden('company_id', ['value'=>$company->id]);
            }elseif(isset($companies)){
                echo $this->Form->control('company_id', ['options' => $companies,'empty'=>'Selecione uma empresa','label'=>'Empresa']);
            }
        ?>
    </fieldset>
    <?= $this->Form->button(__('Salvar')) ?>
    <?= $this->Form->end() ?>
    <?= $this->Form->hidden('cityIdSelected',['value'=>$cityIdSelected])?>
</div>
