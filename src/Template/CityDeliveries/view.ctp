<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CityDelivery $cityDelivery
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Editar cidade para entrega'), ['action' => 'edit', $cityDelivery->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Excluir cidade para entrega'), ['action' => 'delete', $cityDelivery->id], ['confirm' => __('Tem certeza que deseja excluir a empresa para entrega # {0}?', $cityDelivery->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar cidades para entrega'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nova cidade para entrega'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Listar cidades'), ['controller' => 'Cities', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nova cidade'), ['controller' => 'Cities', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Listar empresas'), ['controller' => 'Companies', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nova empresa'), ['controller' => 'Companies', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="cityDeliveries view large-9 medium-8 columns content">
    <h3><?= h('Entrega em '.$cityDelivery->city->name.' - '.$cityDelivery->city->state->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Cidade') ?></th>
            <td><?= $cityDelivery->has('city') ? $this->Html->link($cityDelivery->city->name, ['controller' => 'Cities', 'action' => 'view', $cityDelivery->city->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Estado') ?></th>
            <td><?= $cityDelivery->city->has('state') ? $this->Html->link($cityDelivery->city->state->name, ['controller' => 'States', 'action' => 'view', $cityDelivery->city->state->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Empresa') ?></th>
            <td><?= $cityDelivery->has('company') ? $this->Html->link($cityDelivery->company->name, ['controller' => 'Companies', 'action' => 'view', $cityDelivery->company->id]) : '' ?></td>
        </tr>
    </table>
</div>
