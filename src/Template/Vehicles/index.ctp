<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Vehicle[]|\Cake\Collection\CollectionInterface $vehicles
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Novo veículo'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Listar tipos de veículo'), ['controller'=>'VehicleTypes','action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Listar intervalos'), ['controller'=>'Intervals','action' => 'index']) ?></li>
    </ul>
</nav>
<div class="vehicles index large-9 medium-8 columns content">
    <h3><?= __('Veículos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('company_id',['label'=>'Empresa']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('vehicle_type_id',['label'=>'Tipo veículo']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_available',['label'=>'Disponível']) ?></th>
                <th scope="col" class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($vehicles as $vehicle): ?>
            <tr>
                <td><?= h($vehicle->company->name) ?></td>
                <td><?= h($vehicle->vehicle_type->name) ?></td>
                <td><?= h($vehicle->is_available==1?"Sim":"Não") ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $vehicle->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $vehicle->id]) ?>
                    <?= $this->Form->postLink(__('Excluir'), ['action' => 'delete', $vehicle->id], ['confirm' => __('Tem certeza que deseja excluir o veículo # {0}?', $vehicle->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('primeira')) ?>
            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('próxima') . ' >') ?>
            <?= $this->Paginator->last(__('última') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, exibindo {{current}} registro(s) de um total de {{count}} registros.')]) ?></p>
    </div>
</div>
