<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Vehicle $vehicle
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Form->postLink(
                __('Excluir'),
                ['action' => 'delete', $vehicle->id],
                ['confirm' => __('Tem certeza que deseja excluir o veículo # {0}?', $vehicle->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Listar veículos'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="vehicles form large-9 medium-8 columns content">
    <?= $this->Form->create($vehicle) ?>
    <fieldset>
        <legend><?= __('Editar veículo') ?></legend>
        <?php
            echo $this->Form->control('company_id',['empty'=>'Escolha a empresa','label'=>'Empresa']);
            echo $this->Form->control('vehicle_type_id',['empty'=>'Escolha o tipo de veículo','label'=>'Tipo de veículo']);
            echo $this->Form->control('normal_minimum_value',['label'=>'Mínimo valor normal']);
            echo $this->Form->control('normal_km_value',['label'=>'Valor Km normal']);
            echo $this->Form->control('express_minimum_value',['label'=>'Mínimo valor expresso']);
            echo $this->Form->control('express_km_value',['label'=>'Valor Km expresso']);
            echo $this->Form->control('consider_moving_collection',['label'=>'Considerar deslocamento para coleta']);
            echo $this->Form->control('is_available',['label'=>'Disponível']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Salvar
    ')) ?>
    <?= $this->Form->end() ?>
</div>
