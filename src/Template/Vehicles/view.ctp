<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Vehicle $vehicle
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Editar veículo'), ['action' => 'edit', $vehicle->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Excluir veículo'), ['action' => 'delete', $vehicle->id], ['confirm' => __('Tem certeza que deseja excluir o veículo # {0}?', $vehicle->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar veículos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Novo veículo'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="vehicles view large-9 medium-8 columns content">
    <h3><?= h($vehicle->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Empresa') ?></th>
            <td><?= __($vehicle->company->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tipo de veículo') ?></th>
            <td><?= __($vehicle->vehicle_type->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Mínimo valor normal') ?></th>
            <td><?= $this->Number->format($vehicle->normal_minimum_value,['before'=>'R$ ','places'=>2,'fractionSymbol'=>',','locale'=>'pt_BR']) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Valor Km normal') ?></th>
            <td><?= $this->Number->format($vehicle->normal_km_value,['before'=>'R$ ','places'=>2,'fractionSymbol'=>',','locale'=>'pt_BR']) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Mínimo valor expresso') ?></th>
            <td><?= $this->Number->format($vehicle->express_minimum_value,['before'=>'R$ ','places'=>2,'fractionSymbol'=>',','locale'=>'pt_BR']) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Valor Km expresso') ?></th>
            <td><?= $this->Number->format($vehicle->express_km_value,['before'=>'R$ ','places'=>2,'fractionSymbol'=>',','locale'=>'pt_BR']) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Considerar deslocamento para coleta?') ?></th>
            <td><?= $vehicle->consider_moving_collection ? __('Sim') : __('Não'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Está disponível') ?></th>
            <td><?= $vehicle->is_available ? __('Sim') : __('Não'); ?></td>
        </tr>
    </table>
</div>
