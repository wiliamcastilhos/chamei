<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Company $company
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Editar empresa'), ['action' => 'edit', $company->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Excluir empresa'), ['action' => 'delete', $company->id], ['confirm' => __('Tem certeza que deseja excluir a empresa # {0}?', $company->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar empresas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nova empresa'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="companies view large-9 medium-8 columns content">
    <h3><?= h($company->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nome') ?></th>
            <td><?= h($company->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cnpj') ?></th>
            <td id="cnpj"><?= h($company->cnpj) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('E-mail') ?></th>
            <td><?= h($company->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cep') ?></th>
            <td id="cep"><?= h($company->cep) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Endereço') ?></th>
            <td><?= h($company->address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nº') ?></th>
            <td><?= $this->Number->format($company->street_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Complemento') ?></th>
            <td><?= h($company->complement) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Bairro') ?></th>
            <td><?= h($company->neighborhood) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Telefone Contato') ?></th>
            <td id="contact-phone"><?= h($company->contact_phone) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Telefone Celular') ?></th>
            <td id="cell-phone"><?= h($company->cell_phone) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cidade') ?></th>
            <td><?= h($company->city->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Estado') ?></th>
            <td><?= h($company->city->state->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ativo') ?></th>
            <td><?= $company->is_active==1?"Sim":"Não" ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Hora inicial manhã') ?></th>
            <td><?= h($company->initial_morning_hour->i18nFormat('HH:mm:ss')) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Hora final manhã') ?></th>
            <td><?= h($company->final_morning_hour->i18nFormat('HH:mm:ss')) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Hora inicial tarde') ?></th>
            <td><?= h($company->initial_afternoon_hour->i18nFormat('HH:mm:ss')) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Hora final tarde') ?></th>
            <td><?= h($company->final_afternoon_hour->i18nFormat('HH:mm:ss')) ?></td>
        </tr>
    </table>

</div>
