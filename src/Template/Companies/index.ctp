<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Company[]|\Cake\Collection\CollectionInterface $companies
 */
echo $this->Html->css('companies');
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Nova Empresa'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="companies index large-9 medium-8 columns content">
    <h3><?= __('Empresas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('name',['label'=>'Nome']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('email',['label'=>'E-mail']) ?></th>
                <th class="is-active" scope="col"><?= $this->Paginator->sort('is_active',['label'=>'Ativo']) ?></th>
                <th scope="col" class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($companies as $company): ?>
            <tr>
                <td><?= h($company->name) ?></td>
                <td><?= h($company->email) ?></td>
                <td><?= h($company->is_active==0?"Não":"Sim") ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $company->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $company->id]) ?>
                    <?= $this->Form->postLink(__('Excluir'), ['action' => 'delete', $company->id], ['confirm' => __('Tem certeza que deseja excluir # {0}?', $company->id)]) ?>
                    <?= $this->Html->link(__('Coleta'), ['controller' => 'CityCollections', 'action' => 'add', $company->id]) ?>
                    <?= $this->Html->link(__('Entrega'), ['controller' => 'CityDeliveries', 'action' => 'add', $company->id]) ?>
                    <?= $this->Html->link(__('Calendario'), ['controller' => 'Calendars', 'action' => 'generate', $company->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('primeira')) ?>
            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('próxima') . ' >') ?>
            <?= $this->Paginator->last(__('última') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, exibindo {{current}} registro(s) de um total de {{count}} registros.')]) ?></p>
    </div>
</div>
