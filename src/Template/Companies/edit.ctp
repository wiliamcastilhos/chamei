<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Company $company
 */
$this->Html->script(['https://ajax.googleapis.com/ajax/libs/prototype/1.7.0.0/prototype.js', 'https://ajax.googleapis.com/ajax/libs/scriptaculous/1.8.3/scriptaculous.js']);
echo $this->Html->css('companies');
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Form->postLink(
                __('Excluir'),
                ['action' => 'delete', $company->id],
                ['confirm' => __('Tem certeza que deseja excluir # {0}?', $company->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Listar Empresas'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="companies form large-9 medium-8 columns content">
    <?php 
        echo $this->Form->create($company,['enctype' => 'multipart/form-data']);
    ?>
    <fieldset>
        <legend><?= __('Editar Empresa') ?></legend>
        <?php
            echo $this->Form->control('name', ['label' => 'Nome']);
            echo $this->Form->control('cnpj',['maxlength'=>18]);
            echo $this->Form->control('email', ['label' => 'E-mail','type' => 'email']);
            echo $this->Form->control('cep');
            echo $this->Form->control('address', ['label' => 'Endereço']);
            echo $this->Form->control('street_number', ['label' => 'Nº']);
            echo $this->Form->control('complement', ['label' => 'Complemento']);
            echo $this->Form->control('neighborhood', ['label' => 'Bairro']);
            echo $this->Form->input(
                'states_id', 
                [
                    'type' => 'select',
                    'multiple' => false,
                    'options' => $states, 
                    'empty' => 'Selecione um estado',
                    'onchange' =>'loadCities()',
                    'label' => 'Estado',
                    'required' => true
                ]
            );
            echo '<div class="loading"><b>' . $this->Html->image('load.gif', ['alt' => 'Aguarde, carregando cidades...']) . ' Aguarde, carregando cidades...</b></div>';                
            echo $this->Form->control('city_id', ['label' => 'Cidade','empty'=> 'Selecione uma cidade']);
            echo $this->Form->control('contact_phone', ['label' => 'Telefone Contato']);
            echo $this->Form->control('cell_phone', ['label' => 'Telefone Celular']);
            $fields = [
                'initial_morning_hour'=> ['empty' => true, 'label' => 'Manhã início'],
                'final_morning_hour'=> ['empty' => true, 'label' => 'Manhã fim','validator' => 'validateTime'],
                'initial_afternoon_hour'=> ['empty' => true, 'label' => 'Tarde início'],
                'final_afternoon_hour'=> ['empty' => true, 'label' => 'Tarde fim']
            ];
            echo $this->Form->controls($fields, ['legend' => 'Horários']);
            echo $this->Form->control('is_active',['type' => 'checkbox', 'label'=> 'Ativo']);
            echo $this->Html->image('../uploads/companies/logo/' . $company->logo_dir . '/' . $company->logo);
            if($requiredLogo){
                echo $this->Form->hidden('requiredLogo');
            }
            echo $this->Form->file('logo',['value'=>$company->logo,'required' => false]);
            
        ?>
    </fieldset>
    <?= $this->Form->button(__('Salvar')) ?>
    <?= $this->Form->end() ?>
    <?= $this->Form->hidden('stateIdSelected',['value'=>$stateIdSelected]);?>
    <?= $this->Form->hidden('cityIdSelected',['value'=>$cityIdSelected]);?>
</div>