<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Interval[]|\Cake\Collection\CollectionInterface $intervals
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Novo intervalo'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Listar veículos'), ['controller' => 'Vehicles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Novo veículo'), ['controller' => 'Vehicles', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="intervals index large-9 medium-8 columns content">
    <h3><?= __('Intervalos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('distance_from',['label'=>'Distância de']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('distance_to',['label'=>'Distância até']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('normal_value',['label'=>'Valor normal']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('express_value',['label'=>'Valor expresso']) ?></th>
                <th scope="col"><?= __('Empresa') ?></th>
                <th scope="col"><?= $this->Paginator->sort('vehicle_id',['label'=>'Veículo']) ?></th>
                <th scope="col" class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($intervals as $interval): ?>
            <tr>
                <td><?= $this->Number->format($interval->distance_from) ?></td>
                <td><?= $this->Number->format($interval->distance_to) ?></td>
                <td><?= $this->Number->format($interval->normal_value,['before'=>'R$ ','places'=>2,'fractionSymbol'=>',','locale'=>'pt_BR']) ?></td>
                <td><?= $this->Number->format($interval->express_value,['before'=>'R$ ','places'=>2,'fractionSymbol'=>',','locale'=>'pt_BR']) ?></td>
                <td><?= $interval->has('vehicle') ? $this->Html->link($interval->vehicle->company->name, ['controller' => 'Companies', 'action' => 'view', $interval->vehicle->company->id]) : '' ?></td>
                <td><?= $interval->has('vehicle') ? $this->Html->link($interval->vehicle->vehicle_type->name, ['controller' => 'Vehicles', 'action' => 'view', $interval->vehicle->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $interval->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $interval->id]) ?>
                    <?= $this->Form->postLink(__('Excluir'), ['action' => 'delete', $interval->id], ['confirm' => __('Tem certeza que deseja excluir o intervalo # {0}?', $interval->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('primeira')) ?>
            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('próxima') . ' >') ?>
            <?= $this->Paginator->last(__('última') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, exibindo {{current}} registro(s) de um total de {{count}} registros.')]) ?></p>
    </div>
</div>
