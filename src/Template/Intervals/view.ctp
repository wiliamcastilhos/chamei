<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Interval $interval
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Editar intervalo'), ['action' => 'edit', $interval->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Excluir intervalo'), ['action' => 'delete', $interval->id], ['confirm' => __('Tem certeza que deseja excluir o intervalo # {0}?', $interval->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar intervalos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Novo intervalo'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Listar veículos'), ['controller' => 'Vehicles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Novo veículo'), ['controller' => 'Vehicles', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="intervals view large-9 medium-8 columns content">
    <h3><?= h($interval->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Empresa') ?></th>
            <td><?= $interval->has('vehicle') ? $this->Html->link($interval->vehicle->company->name, ['controller' => 'Companies', 'action' => 'view', $interval->vehicle->company->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Veículo') ?></th>
            <td><?= $interval->has('vehicle') ? $this->Html->link($interval->vehicle->vehicle_type->name, ['controller' => 'Vehicles', 'action' => 'view', $interval->vehicle->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Distância de') ?></th>
            <td><?= $this->Number->format($interval->distance_from,['after'=>' Km']) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Distância até') ?></th>
            <td><?= $this->Number->format($interval->distance_to,['after'=>' Km']) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Valor normal') ?></th>
            <td><?= $this->Number->format($interval->normal_value,['before'=>'R$ ','places'=>2,'fractionSymbol'=>',','locale'=>'pt_BR']) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Valor expresso') ?></th>
            <td><?= $this->Number->format($interval->express_value,['before'=>'R$ ','places'=>2,'fractionSymbol'=>',','locale'=>'pt_BR']) ?></td>
        </tr>
    </table>
</div>
