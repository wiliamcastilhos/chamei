<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Interval $interval
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Form->postLink(
                __('Excluir'),
                ['action' => 'delete', $interval->id],
                ['confirm' => __('Tem certeza que deseja excluir o intervalo # {0}?', $interval->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Listar intervalos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Listar veiculos'), ['controller' => 'Vehicles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Novo veículo'), ['controller' => 'Vehicles', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="intervals form large-9 medium-8 columns content">
    <?= $this->Form->create($interval) ?>
    <fieldset>
        <legend><?= __('Editar intervalo') ?></legend>
        <?php
            echo $this->Form->control('company_id',['options'=>$companies,'label' => 'Empresa','empty'=>'Selecione a empresa','onchange' =>'loadVehicles()',]);
            echo '<div class="loading"><b>' . $this->Html->image('load.gif', ['alt' => 'Aguarde, carregando veículos...']) . ' Aguarde, carregando veículos...</b></div>';
            echo $this->Form->control('vehicle_id', ['empty'=>'Selecione o veículo','label'=>'Veículo']);
            echo $this->Form->control('distance_from',['label'=>'Distância de']);
            echo $this->Form->control('distance_to',['label'=>'Distância até']);
            echo $this->Form->control('normal_value',['label'=>'Valor normal']);
            echo $this->Form->control('express_value',['label'=>'Valor expresso']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Salvar')) ?>
    <?= $this->Form->end() ?>
    <?= $this->Form->hidden('companyIdSelected',['value'=>$companyIdSelected]);?>
    <?= $this->Form->hidden('vehicleIdSelected',['value'=>$vehicleIdSelected]);?>
</div>
<?php echo $this->Html->scriptBlock('
    function loadVehicles(){
        $(".loading").toggle(50,function(){$("#vehicle-id").css("visibility","hidden");});
        var dataSet = {companyId: $("#company-id").val()};
        var requestUrl = "/myapp/vehicles/findByCompany";
        $.ajax({
            type: "POST",
            url: requestUrl,
            data: dataSet,
            success: function(result) {
                $("#vehicle-id").html(result);
                $(".loading").toggle(50,function(){$("#vehicle-id").css("visibility","visible");});
                selectVehicle();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }

    if($("input[name=companyIdSelected]").val() != "" && $("input[name=companyIdSelected]").val() != undefined){
        $("#company-id > option").each(
            function() {
                if(this.value == $("input[name=companyIdSelected]").val()){
                    $(this).attr("selected","selected");
                    $("#company-id").trigger("change");
                }
            }
        );
    }

    function selectVehicle(){
        if($("input[name=vehicleIdSelected]").val() != "" && $("input[name=vehicleIdSelected]").val() != undefined){
            $("#vehicle-id > option").each(function() {
                if(this.value == $("input[name=vehicleIdSelected]").val()){
                    $(this).attr("selected","selected");
                    $("#vehicle-id").trigger("change");
                }
            });
        }
    }
    ', ['defer' => false]);?>