<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Calendar[]|\Cake\Collection\CollectionInterface $calendars
 */
$weekDay = [
    'Domingo',
    'Segunda',
    'Terça',
    'Quarta',
    'Quinta',
    'Sexta',
    'Sábado'
];
echo $this->Html->script(['jquery-ui','datepicker-pt-BR']);
echo $this->Html->css('jquery-ui.css');
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Gerar novo calendário'), ['action' => 'generate']) ?></li>
        <li><?= $this->Html->link(__('Listar empresas'), ['controller' => 'Companies', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nova empresa'), ['controller' => 'Companies', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="calendars index large-9 medium-8 columns content">
    <h3><?= __('Calendários') ?></h3>

    <?php 
    echo $this->Form->create(null, ['valueSources' => 'query']);
    echo $this->Form->control('year',['label'=>'Ano']);
    echo $this->Form->input(
                'company_id', 
                [
                    'type' => 'select',
                    'multiple' => false,
                    'options' => $companies, 
                    'empty' => 'Selecione uma empresa',
                    'label' => 'Empresa',
                    'required' => false
                ]
            );
    echo $this->Form->control('dateFormat',['label'=>'Data']);
    echo $this->Form->hidden('date');
    // echo $this->Form->control('save_query', ['type' => 'checkbox','label'=>'Salvar query[TESTE]']);
    echo $this->Form->button('Filter', ['type' => 'submit']);
    echo $this->Html->link('Reset', ['action' => 'index']);
    echo $this->Form->end();
    ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('company_id',['label'=>'Empresa']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('year',['label'=>'Ano']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('date',['label'=>'Data']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_util_day',['label'=>'Dia útil']) ?></th>
                <th scope="col"><?= __('Dia semana') ?></th>
                <th scope="col" class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($calendars as $calendar): ?>
            <tr>
                <td><?= $calendar->has('company') ? $this->Html->link($calendar->company->name, ['controller' => 'Companies', 'action' => 'view', $calendar->company->id]) : '' ?></td>
                <td><?= h($calendar->year) ?></td>
                <td><?= h($calendar->date->format('d/m/Y')) ?></td>
                <td><?= h($calendar->is_util_day==1?"Sim":"Não") ?></td>
                <td><?= h($weekDay[$calendar->date->format('w')]) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $calendar->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $calendar->id]) ?>
                    <?= $this->Form->postLink(__('Excluir'), ['action' => 'delete', $calendar->id], ['confirm' => __('Tem certeza que deseja excluir o calendário # {0}?', $calendar->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('primeira')) ?>
            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('próxima') . ' >') ?>
            <?= $this->Paginator->last(__('última') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, exibindo {{current}} registro(s) de um total de {{count}} registros.')]) ?></p>
    </div>
</div>
<?php echo $this->Html->scriptBlock('
    $( "#dateformat" ).datepicker({
      changeMonth: true,
      changeYear: true,
      onSelect: function(dateText, inst){
        $("input[name=date]").val(dateText.substring(6)+"-"+dateText.substring(3,5)+"-"+dateText.substring(0,2));
      }
    });
', ['defer' => false]);?>