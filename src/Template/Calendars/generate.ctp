<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Calendar $calendar
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Listar calendários'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Listar empresas'), ['controller' => 'Companies', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nova empresa'), ['controller' => 'Companies', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="calendars form large-9 medium-8 columns content">
    <?= $this->Form->create($calendar) ?>
    <fieldset>
        <legend><?= __('Gerar calendário') ?></legend>
        <?php
            if(isset($company_id)){
                echo $this->Form->hidden('company_id',['value'=>$company_id]);
            }elseif(isset($companies)){
                echo $this->Form->control('company_id', ['options' => $companies]);
            }
            echo $this->Form->control('year',['min' => date('Y')]);
            $fields = [
                'sunday' => ['type' => 'checkbox', 'empty' => true, 'label'=> 'Domingo'],
                'monday' => ['type' => 'checkbox', 'empty' => true, 'label'=> 'Segunda'],
                'tuesday' => ['type' => 'checkbox', 'empty' => true, 'label'=> 'Terça'],
                'wednesday' => ['type' => 'checkbox', 'empty' => true, 'label'=> 'Quarta'],
                'thursday' => ['type' => 'checkbox', 'empty' => true, 'label'=> 'Quinta'],
                'friday' => ['type' => 'checkbox', 'empty' => true, 'label'=> 'Sexta'],
                'saturday' => ['type' => 'checkbox', 'empty' => true, 'label'=> 'Sábado']
            ];
            echo $this->Form->controls($fields, ['legend' => 'Dias úteis']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
