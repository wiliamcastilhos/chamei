<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Calendar $calendar
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Form->postLink(
                __('Excluir'),
                ['action' => 'delete', $calendar->id],
                ['confirm' => __('Tem certeza que deseja excluir o calendário # {0}?', $calendar->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Listar calendários'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Listar empresas'), ['controller' => 'Companies', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nova empresa'), ['controller' => 'Companies', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="calendars form large-9 medium-8 columns content">
    <?= $this->Form->create($calendar) ?>
    <fieldset>
        <legend><?= __('Editar calendário') ?></legend>
        <?php
            echo $this->Form->control('company_id', ['options' => $companies,'label'=>'Empresa','empty'=>'Selecione uma empresa']);
            echo $this->Form->control('year',['label'=>'Ano']);
            echo $this->Form->control('date',['label'=>'Data']);
            echo $this->Form->control('initial_morning_hour',['label'=>'Hora inicial manhã']);
            echo $this->Form->control('final_morning_hour',['label'=>'Hora final manhã']);
            echo $this->Form->control('initial_afternoon_hour',['label'=>'Hora inicial tarde']);
            echo $this->Form->control('final_afternoon_hour',['label'=>'Hora final tarde']);
            echo $this->Form->control('is_util_day',['label'=>'Dia útil']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Salvar')) ?>
    <?= $this->Form->end() ?>
</div>
