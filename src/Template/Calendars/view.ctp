<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Calendar $calendar
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Editar calendário'), ['action' => 'edit', $calendar->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Excluir calendário'), ['action' => 'delete', $calendar->id], ['confirm' => __('Are you sure you want to delete # {0}?', $calendar->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar calendários'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Novo calendário'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Listar empresas'), ['controller' => 'Companies', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nova empresa'), ['controller' => 'Companies', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="calendars view large-9 medium-8 columns content">
    <h3><?= h($calendar->date->format('d/m/Y')) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Empresa') ?></th>
            <td><?= $calendar->has('company') ? $this->Html->link($calendar->company->name, ['controller' => 'Companies', 'action' => 'view', $calendar->company->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ano') ?></th>
            <td><?= __($calendar->year) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Hora inicial manhã') ?></th>
            <td><?= h($calendar->initial_morning_hour->format('H:i')) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Hora final manhã') ?></th>
            <td><?= h($calendar->final_morning_hour->format('H:i')) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Hora inicial tarde') ?></th>
            <td><?= h($calendar->initial_afternoon_hour->format('H:i')) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Hora final tarde') ?></th>
            <td><?= h($calendar->final_afternoon_hour->format('H:i')) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dia útil') ?></th>
            <td><?= $calendar->is_util_day ? __('Sim') : __('Não'); ?></td>
        </tr>
    </table>
</div>
