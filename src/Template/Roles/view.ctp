<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Role $role
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Editar tipo de usuário'), ['action' => 'edit', $role->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Excluir tipo de usuário'), ['action' => 'delete', $role->id], ['confirm' => __('Tem certeza que deseja excluir o tipo de usuário # {0}?', $role->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar tipos de usuários'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Novo tipo de usuário'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="roles view large-9 medium-8 columns content">
    <h3><?= h($role->role) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($role->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Criado') ?></th>
            <td><?= h($role->created->i18nFormat('dd/MM/yyyy HH:mm:ss')) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modificado') ?></th>
            <td><?= h($role->modified->i18nFormat('dd/MM/yyyy HH:mm:ss')) ?></td>
        </tr>
    </table>
</div>
