<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\VehicleType[]|\Cake\Collection\CollectionInterface $vehicleTypes
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Novo tipo de veículo'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Listar veículos'), ['controller' => 'Vehicles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Novo veículo'), ['controller' => 'Vehicles', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="vehicleTypes index large-9 medium-8 columns content">
    <h3><?= __('Tipos de veículo') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('name',['label'=>'Nome']) ?></th>
                <th scope="col" class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($vehicleTypes as $vehicleType): ?>
            <tr>
                <td><?= h($vehicleType->name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $vehicleType->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $vehicleType->id]) ?>
                    <?= $this->Form->postLink(__('Excluir'), ['action' => 'delete', $vehicleType->id], ['confirm' => __('Tem certeza que deseja excluir o tipo de veículo # {0}?', $vehicleType->name)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('primeira')) ?>
            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('próxima') . ' >') ?>
            <?= $this->Paginator->last(__('última') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, exibindo {{current}} registro(s) de um total de {{count}}')]) ?></p>
    </div>
</div>
