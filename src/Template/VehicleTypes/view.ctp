<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\VehicleType $vehicleType
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Editar tipo de veículo'), ['action' => 'edit', $vehicleType->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Excluir tipo de veículo'), ['action' => 'delete', $vehicleType->id], ['confirm' => __('Tem certeza que deseja excluir o tipo de veículo # {0}?', $vehicleType->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar tipos de veículos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Novo tipo de veículo'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Listar veículos'), ['controller' => 'Vehicles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Novo veículo'), ['controller' => 'Vehicles', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="vehicleTypes view large-9 medium-8 columns content">
    <h3><?= h($vehicleType->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name',['label'=>'Nome']) ?></th>
            <td><?= h($vehicleType->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($vehicleType->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Veículos relacionados') ?></h4>
        <?php if (!empty($vehicleType->vehicles)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Company Id',['label'=>'Empresa']) ?></th>
                <th scope="col"><?= __('Vehicle Type Id',['label'=>'Tipo de veículo']) ?></th>
                <th scope="col"><?= __('Disponível') ?></th>
                <th scope="col" class="actions"><?= __('Ações') ?></th>
            </tr>
            <?php foreach ($vehicleType->vehicles as $vehicles): ?>
            <tr>
                <td><?= h($vehicles->company->name) ?></td>
                <td><?= h($vehicles->vehicle_type->name) ?></td>
                <td><?= h($vehicles->is_available==1?'Sim':'Não') ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Visualizar'), ['controller' => 'Vehicles', 'action' => 'view', $vehicles->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['controller' => 'Vehicles', 'action' => 'edit', $vehicles->id]) ?>
                    <?= $this->Form->postLink(__('Excluir'), ['controller' => 'Vehicles', 'action' => 'delete', $vehicles->id], ['confirm' => __('Tem certeza que deseja excluir o veículo # {0}?', $vehicles->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
