<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\VehicleType $vehicleType
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Listar tipos de veículo'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Listar veículos'), ['controller' => 'Vehicles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Novo veículo'), ['controller' => 'Vehicles', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="vehicleTypes form large-9 medium-8 columns content">
    <?= $this->Form->create($vehicleType) ?>
    <fieldset>
        <legend><?= __('Adicionar tipo de veículo') ?></legend>
        <?php
            echo $this->Form->control('name',['label'=>'Nome']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Salvar')) ?>
    <?= $this->Form->end() ?>
</div>
