<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DeliveryCity Controller
 *
 * @property \App\Model\Table\DeliveryCityTable $DeliveryCity
 *
 * @method \App\Model\Entity\DeliveryCity[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DeliveryCityController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Cities', 'Companies']
        ];
        $deliveryCity = $this->paginate($this->DeliveryCity);

        $this->set(compact('deliveryCity'));
    }

    /**
     * View method
     *
     * @param string|null $id Delivery City id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $deliveryCity = $this->DeliveryCity->get($id, [
            'contain' => ['Cities', 'Companies']
        ]);

        $this->set('deliveryCity', $deliveryCity);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $deliveryCity = $this->DeliveryCity->newEntity();
        if ($this->request->is('post')) {
            $deliveryCity = $this->DeliveryCity->patchEntity($deliveryCity, $this->request->getData());
            if ($this->DeliveryCity->save($deliveryCity)) {
                $this->Flash->success(__('The delivery city has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The delivery city could not be saved. Please, try again.'));
        }
        $cities = $this->DeliveryCity->Cities->find('list', ['limit' => 200]);
        $companies = $this->DeliveryCity->Companies->find('list', ['limit' => 200]);
        $this->set(compact('deliveryCity', 'cities', 'companies'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Delivery City id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $deliveryCity = $this->DeliveryCity->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $deliveryCity = $this->DeliveryCity->patchEntity($deliveryCity, $this->request->getData());
            if ($this->DeliveryCity->save($deliveryCity)) {
                $this->Flash->success(__('The delivery city has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The delivery city could not be saved. Please, try again.'));
        }
        $cities = $this->DeliveryCity->Cities->find('list', ['limit' => 200]);
        $companies = $this->DeliveryCity->Companies->find('list', ['limit' => 200]);
        $this->set(compact('deliveryCity', 'cities', 'companies'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Delivery City id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $deliveryCity = $this->DeliveryCity->get($id);
        if ($this->DeliveryCity->delete($deliveryCity)) {
            $this->Flash->success(__('The delivery city has been deleted.'));
        } else {
            $this->Flash->error(__('The delivery city could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
