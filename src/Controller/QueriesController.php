<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Network\Http\Client;
use Cake\I18n\Number;
use App\Controller\QueryExpression;

/**
 * Queries Controller
 *
 * @property \App\Model\Table\QueriesTable $Queries
 *
 * @method \App\Model\Entity\Query[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class QueriesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['CityCollections', 'CityDeliveries']
        ];
        $queries = $this->paginate($this->Queries);

        $this->set(compact('queries'));
    }

    /**
     * View method
     *
     * @param string|null $id Query id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $query = $this->Queries->get($id, [
            'contain' => ['CityCollections', 'CityDeliveries', 'QuerySolicitations']
        ]);

        $this->set('query', $query);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->Flash->error(__('Não é permitido incluir histórico de consulta.'));
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Query id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->Flash->error(__('Não é permitido editar um histórico de consulta.'));
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Query id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $query = $this->Queries->get($id);
        if ($this->Queries->delete($query)) {
            $this->Flash->success(__('The query has been deleted.'));
        } else {
            $this->Flash->error(__('The query could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function query()
    {
        $query = $this->Queries->newEntity();
        if ($this->request->is('post')) {
            $this->saveQuery();
            $budgets = $this->calculate();
            $this->set('budgets',$budgets);
        }
        $this->loadModel('States');
        $states = $this->States->find('all');
        foreach($states->toArray() as $key=>$value){
            $ufStates[$value->id] = $value->uf;
        }
        $this->loadModel('VehicleTypes');
        $vehicleTypes = $this->VehicleTypes->find('list');
        $this->set(compact('query', 'ufStates','vehicleTypes','budgets'));
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow('findCity');
    }

    private function saveQuery(){
        $saved = false;
        $query =  $this->Queries->newEntity($this->request->getData());
        $query->query_vehicle_types = $this->request->getData('vehicleTypes');
        if ($this->Queries->save($query)) {
            $this->loadModel('QueryVehicleTypes');
            //Vehicle Types
            $vehicle_types = $query->query_vehicle_types;
            foreach($vehicle_types['id'] as $id){
                $queryVehicleTypes = $this->QueryVehicleTypes->newEntity();
                $queryVehicleTypes = $this->QueryVehicleTypes->patchEntity($queryVehicleTypes, ['vehicle_type_id'=>$id,'query_id'=>$query->id]);
                if($this->QueryVehicleTypes->save($queryVehicleTypes)){
                    $saved = true;
                }
            }
        }
        if(!$saved){
            $this->Flash->error(__('Erro ao salvar query'));
        }
    }

    private function calculate(){
        $vehicleTypes = $this->request->getData('vehicleTypes');
        $distance = $this->request->getData("distance")/1000;
        $this->loadModel('Intervals');
        $intervals = $this->Intervals->find('all')
            ->where(['distance_from <= '.$distance,
                    'distance_to >= '.$distance,
                    'Companies.is_active = 1',
                    'Vehicles.vehicle_type_id in ('.implode($vehicleTypes['id'],',').')'])
            ->contain(['Vehicles'=>'Companies']);
        $budgets = array();
        //Primeiro busca se há intervalos para considerar, senão busca os veículos que atendem essa rota.
        foreach($intervals->toArray() as $interval){
            if(($distance * $interval->normal_value) > $interval->vehicle->normal_minimum_value){
                $normalValue = $distance * $interval->normal_value;
            }else{
                $normalValue = $interval->vehicle->normal_minimum_value;
            }

            if(($distance * $interval->express_value) > $interval->vehicle->express_minimum_value){
                $expressValue = $distance * $interval->express_value;
            }else{
                $expressValue = $interval->vehicle->express_minimum_value;
            }
            $values = [
                'normalValue'=>$normalValue,
                'expressValue'=>$expressValue,
                'logo'=>$interval->vehicle->company->logo_dir.'/'.$interval->vehicle->company->logo,
                'companyID'=>$interval->vehicle->company->id
            ];
            $budgets[]= $values;
        }
        if(empty($budgets)){
            $this->loadModel('Vehicles');
            $vehicles = $this->Vehicles->find('all')
            ->where(['is_available = 1',
                    'Companies.is_active = 1',
                    'Vehicles.vehicle_type_id in ('.implode($vehicleTypes['id'],',').')'])
            ->contain(['Companies']);
            foreach($vehicles->toArray() as $vehicle){
                if(($distance * $vehicle->normal_km_value) > $vehicle->normal_minimum_value){
                    $normalValue = $distance * $vehicle->normal_km_value;
                }else{
                    $normalValue = $vehicle->normal_minimum_value;
                }
                if(($distance * $vehicle->express_km_value) > $vehicle->express_minimum_value){
                    $expressValue = $distance * $vehicle->express_km_value;
                }else{
                    $expressValue = $vehicle->express_minimum_value;
                }
                $values = [
                    'normalValue'=>$normalValue,
                    'expressValue'=>$expressValue,
                    'logo'=>$vehicle->company->logo_dir.'/'.$vehicle->company->logo,
                    'companyID'=>$vehicle->company->id
                ];
                $budgets[]= $values;
            }
        }
        return $budgets;
    }

    private function getCityAndState($cityId){
        $this->loadModel('Cities');
        $response = $this->Cities->get($cityId,['contain'=>'States']);
        return $response->name." - ".$response->state->uf;
    }
}
