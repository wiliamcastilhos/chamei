<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Calendars Controller
 *
 * @property \App\Model\Table\CalendarsTable $Calendars
 *
 * @method \App\Model\Entity\Calendar[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CalendarsController extends AppController
{

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Search.Prg', [
            'actions' => ['index', 'lookup']
        ]);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $query = $this->Calendars
            ->find('search', ['search' => $this->request->getQueryParams()])
            ->contain(['Companies']);

        $companies = $this->Calendars->Companies->find('list');
        $calendars = $this->paginate($query);
        $this->set(compact('calendars','companies'));

    }

    /**
     * View method
     *
     * @param string|null $id Calendar id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $calendar = $this->Calendars->get($id, [
            'contain' => ['Companies']
        ]);

        $this->set('calendar', $calendar);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $calendar = $this->Calendars->newEntity();
        if ($this->request->is('post')) {
            $calendar = $this->Calendars->patchEntity($calendar, $this->request->getData());
            if ($this->Calendars->save($calendar)) {
                $this->Flash->success(__('The calendar has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The calendar could not be saved. Please, try again.'));
        }
        $companies = $this->Calendars->Companies->find('list', ['limit' => 200]);
        $this->set(compact('calendar', 'companies'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Calendar id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $calendar = $this->Calendars->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $calendar = $this->Calendars->patchEntity($calendar, $this->request->getData());
            if ($this->Calendars->save($calendar)) {
                $this->Flash->success(__('The calendar has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The calendar could not be saved. Please, try again.'));
        }
        $companies = $this->Calendars->Companies->find('list', ['limit' => 200]);
        $this->set(compact('calendar', 'companies'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Calendar id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $calendar = $this->Calendars->get($id);
        if ($this->Calendars->delete($calendar)) {
            $this->Flash->success(__('O calendário foi excluído.'));
        } else {
            $this->Flash->error(__('O calendário não pode ser salvo. Por favor tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function generate($company_id = null)
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();

            $resultQuery = $this->Calendars->find('all')->contain('Companies')->where(['Calendars.year' => $data['year'], 'Calendars.company_id'=> $data['company_id']])->first();
            if($resultQuery != null){
                /**
                    Não pode ser gerado várias vezes o mesmo calendário para uma mesma empresa.
                */
                $this->Flash->error(__('Já há um calendário gerado para a empresa '.$resultQuery->company->name.' e para o ano '.$resultQuery->year));
            }else{
                $calendarToSave = array();
                // $calendar = $this->Calendars->patchEntity($calendar, $data);
                $ini =  new \DateTime($data['year'].'-01-01');
                $end = new \DateTime($data['year'].'-12-31');
                $end->modify('+1 day');

                $workingDays = [
                    $data['sunday'],
                    $data['monday'],
                    $data['tuesday'],
                    $data['wednesday'],
                    $data['thursday'],
                    $data['friday'],
                    $data['saturday']
                ];

                $interval = new \DateInterval('P1D');
                $period = new \DatePeriod($ini, $interval ,$end);
                $daysSaved = array();

                foreach($period as $dateTime){
                    $company = $this->Calendars->Companies->find('all')->where(['Companies.id ' => $data['company_id']])->first();
                    $dayWeek = $dateTime->format('w');

                    $calendarToSave['company_id'] = $data['company_id'];
                    $calendarToSave['year'] = $data['year'];
                    $calendarToSave['is_util_day'] = $workingDays[$dayWeek];
                    $calendarToSave['date'] = $dateTime;
                    $calendarToSave['initial_morning_hour'] = $company->initial_morning_hour;
                    $calendarToSave['final_morning_hour'] = $company->final_morning_hour;
                    $calendarToSave['initial_afternoon_hour'] = $company->initial_afternoon_hour;
                    $calendarToSave['final_afternoon_hour'] = $company->final_afternoon_hour;
                    $calendar = $this->Calendars->newEntity($calendarToSave);
                    if($this->Calendars->save($calendar)){
                        array_push($daysSaved, $dateTime->format('d-M-Y'));
                    }else{
                        debug($calendar);
                        return false;
                    }
                }
                $this->Flash->success(__('Calendário salvo.'));
                return $this->redirect(['action' => 'index']);
            }
        }
        if($company_id != null){
            $company = $this->Calendars->Companies->find('all')->where(['Companies.id ' => $company_id])->first();
            $this->set('company_id',$company->id);
        }else{
            $this->set('companies',$this->Calendars->Companies->find('list', ['limit' => 200]));
        }
        $calendar = $this->Calendars->newEntity();
        $this->set('calendar',$calendar);
    }

    public function saveQuery(){
        $this->loadModel('QueryHistories');
        $this->QueryHistories->saveQuery();
    }

}
