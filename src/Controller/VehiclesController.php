<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Vehicles Controller
 *
 * @property \App\Model\Table\VehiclesTable $Vehicles
 *
 * @method \App\Model\Entity\Vehicle[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class VehiclesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Companies', 'VehicleTypes']
        ];
        $vehicles = $this->paginate($this->Vehicles);

        $this->set(compact('vehicles'));
    }

    /**
     * View method
     *
     * @param string|null $id Vehicle id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $vehicle = $this->Vehicles->get($id, [
            'contain' => ['Companies', 'VehicleTypes', 'Intervals']
        ]);

        $this->set('vehicle', $vehicle);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $vehicle = $this->Vehicles->newEntity();
        if ($this->request->is('post')) {
            $vehicle = $this->Vehicles->patchEntity($vehicle, $this->request->getData());
            if ($this->Vehicles->save($vehicle)) {
                $this->Flash->success(__('Veiculo adicionado com sucesso.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Veículo não foi salvo. Por favor, tente novamente.'));
        }
        $companies = $this->Vehicles->Companies->find('list', ['limit' => 200]);
        $vehicleTypes = $this->Vehicles->VehicleTypes->find('list', ['limit' => 200]);
        $this->set(compact('vehicle', 'companies', 'vehicleTypes'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Vehicle id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $vehicle = $this->Vehicles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $vehicle = $this->Vehicles->patchEntity($vehicle, $this->request->getData());
            if ($this->Vehicles->save($vehicle)) {
                $this->Flash->success(__('Veículo alterado com sucesso.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('O veículo não foi atualizado. Favor tentar novamente.'));
        }
        $companies = $this->Vehicles->Companies->find('list', ['limit' => 200]);
        $vehicleTypes = $this->Vehicles->VehicleTypes->find('list', ['limit' => 200]);
        $this->set(compact('vehicle', 'companies', 'vehicleTypes'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Vehicle id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $vehicle = $this->Vehicles->get($id);
        if ($this->Vehicles->delete($vehicle)) {
            $this->Flash->success(__('O veículo foi excluído.'));
        } else {
            $this->Flash->error(__('O veículo não foi excluído. Por favor, tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function findByCompany(){
        $this->autoRender = false;   
        if ($this->request->is('ajax')) {
            $vehicles = $this->Vehicles->find()->where(['company_id' => $this->request->data['companyId']])->contain('VehicleTypes');
            if($vehicles->isEmpty()){
                $data = '<option disabled value="">Não há veículos cadastrados para essa empresa.</option>';
            }else{
                $data = '<option value="">Selecione o veículo</option>';
                foreach($vehicles as $vehicle) {
                    $data .= '<option value="'.$vehicle->id.'">'.$vehicle->vehicle_type->name.'</option>';
                }    
            }
            
            echo $data;
        }
    }
}
