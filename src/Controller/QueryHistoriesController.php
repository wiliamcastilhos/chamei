<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * QueryHistories Controller
 *
 * @property \App\Model\Table\QueryHistoriesTable $QueryHistories
 *
 * @method \App\Model\Entity\QueryHistory[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class QueryHistoriesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Companies', 'CityCollections', 'CityDeliveries', 'Users']
        ];
        $queryHistories = $this->paginate($this->QueryHistories);

        $this->set(compact('queryHistories'));
    }

    /**
     * View method
     *
     * @param string|null $id Query History id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $queryHistory = $this->QueryHistories->get($id, [
            'contain' => ['Companies', 'CityCollections', 'CityDeliveries', 'Users']
        ]);

        $this->set('queryHistory', $queryHistory);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $queryHistory = $this->QueryHistories->newEntity();
        if ($this->request->is('post')) {
            $queryHistory = $this->QueryHistories->patchEntity($queryHistory, $this->request->getData());
            if ($this->QueryHistories->save($queryHistory)) {
                $this->Flash->success(__('The query history has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The query history could not be saved. Please, try again.'));
        }
        $companies = $this->QueryHistories->Companies->find('list', ['limit' => 200]);
        $cityCollections = $this->QueryHistories->CityCollections->find('list', ['limit' => 200]);
        $cityDeliveries = $this->QueryHistories->CityDeliveries->find('list', ['limit' => 200]);
        $users = $this->QueryHistories->Users->find('list', ['limit' => 200]);
        $this->set(compact('queryHistory', 'companies', 'cityCollections', 'cityDeliveries', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Query History id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $queryHistory = $this->QueryHistories->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $queryHistory = $this->QueryHistories->patchEntity($queryHistory, $this->request->getData());
            if ($this->QueryHistories->save($queryHistory)) {
                $this->Flash->success(__('The query history has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The query history could not be saved. Please, try again.'));
        }
        $companies = $this->QueryHistories->Companies->find('list', ['limit' => 200]);
        $cityCollections = $this->QueryHistories->CityCollections->find('list', ['limit' => 200]);
        $cityDeliveries = $this->QueryHistories->CityDeliveries->find('list', ['limit' => 200]);
        $users = $this->QueryHistories->Users->find('list', ['limit' => 200]);
        $this->set(compact('queryHistory', 'companies', 'cityCollections', 'cityDeliveries', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Query History id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $queryHistory = $this->QueryHistories->get($id);
        if ($this->QueryHistories->delete($queryHistory)) {
            $this->Flash->success(__('The query history has been deleted.'));
        } else {
            $this->Flash->error(__('The query history could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function saveQuery()
    {
	debug(':-)');
    }
}
