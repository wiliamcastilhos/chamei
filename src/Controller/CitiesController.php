<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Cities Controller
 *
 * @property \App\Model\Table\CitiesTable $Cities
 *
 * @method \App\Model\Entity\City[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CitiesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['States']
        ];
        $cities = $this->paginate($this->Cities);

        $this->set(compact('cities'));
    }

    /**
     * View method
     *
     * @param string|null $id City id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $city = $this->Cities->get($id, [
            'contain' => ['States', 'Companies']
        ]);

        $this->set('city', $city);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $city = $this->Cities->newEntity();
        if ($this->request->is('post')) {
            $city = $this->Cities->patchEntity($city, $this->request->getData());
            if ($this->Cities->save($city)) {
                $this->Flash->success(__('Cidade salva.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Não foi possível salvar a cidade.'));
        }
        $states = $this->Cities->States->find('list', ['limit' => 200]);
        $this->set(compact('city', 'states'));
    }

    /**
     * Edit method
     *
     * @param string|null $id City id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $city = $this->Cities->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $city = $this->Cities->patchEntity($city, $this->request->getData());
            if ($this->Cities->save($city)) {
                $this->Flash->success(__('Cidade atualizada.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Não foi possível atualizar a cidade.'));
        }
        $states = $this->Cities->States->find('list', ['limit' => 200]);
        $this->set(compact('city', 'states'));
    }

    /**
     * Delete method
     *
     * @param string|null $id City id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $city = $this->Cities->get($id);
        if ($this->Cities->delete($city)) {
            $this->Flash->success(__('Cidade excluída.'));
        } else {
            $this->Flash->error(__('A cidade não pode ser excluída.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function list()
    {
        $this -> autoRender = false;   
        if ($this->request->is('ajax')) {
            $cities = $this->Cities->find()->where(['state_id' => $this->request->data['stateId']])->order(['name' => 'ASC']);
            $data = '<option value="">Selecione uma cidade</option>';
            foreach($cities as $city) {
                $data .= '<option value="'.$city->id.'">'.$city->name.'</option>';
            }
            echo $data;
        }
    }

    public function findStateAndCity()
    {
        $this -> autoRender = false;   
        if ($this->request->is('ajax')) {
            $this->loadModel('States');

            $states = $this->States->find()->where();
            $statesOption = '<option value="">Selecione um estado</option>';
            $stateIdSelect = -1;

            foreach($states as $state) {
                if($state->uf == strtoupper($this->request->data['nmUf'])){
                    $statesOption .= '<option selected value="'.$state->id.'">';
                    if(isset($this->request->data['displayUf']) && $this->request->data['displayUf'] != null){
                        $statesOption .= $state->uf.'</option>';
                    }else{
                        $statesOption .= $state->name.'</option>';    
                    }
                    $stateIdSelect = $state->id;
                }else{
                    $statesOption .= '<option value="'.$state->id.'">';
                    if(isset($this->request->data['displayUf']) && $this->request->data['displayUf'] != null){
                        $statesOption .= $state->uf.'</option>';
                    }else{
                        $statesOption .= $state->name.'</option>';    
                    }
                }
            }

            $cities = $this->Cities->find()->where(['state_id' => $stateIdSelect]);
            $citiesOption = '<option value="">Selecione uma cidade</option>';
            $cityIdSelected = -1;
            foreach($cities as $city) {
                if($city->name == $this->request->data['nmCity']){
                    $citiesOption .= '<option selected value="'.$city->id.'">'.$city->name.'</option>';
                }else{
                    $citiesOption .= '<option value="'.$city->id.'">'.$city->name.'</option>';
                }
            }
            $result = json_encode(['states'=>$statesOption, 'cities'=>$citiesOption]);
            $this->response->type('json');
            $this->response->body($result);
            return $this->response;
        }
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow('findStateAndCity');
    }
}
