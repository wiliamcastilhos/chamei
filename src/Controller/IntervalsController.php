<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Intervals Controller
 *
 * @property \App\Model\Table\IntervalsTable $Intervals
 *
 * @method \App\Model\Entity\Interval[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class IntervalsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Vehicles'=>['VehicleTypes','Companies']]
        ];
        $intervals = $this->paginate($this->Intervals);

        $this->set(compact('intervals'));
    }

    /**
     * View method
     *
     * @param string|null $id Interval id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $interval = $this->Intervals->get($id, [
            'contain' => ['Vehicles'=>['VehicleTypes','Companies']]
        ]);
        $companies = $this->Intervals->Vehicles->Companies->find('list');

        $this->set(compact('interval', 'companies'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $interval = $this->Intervals->newEntity();
        if ($this->request->is('post')) {
            $interval = $this->Intervals->patchEntity($interval, $this->request->getData());
            if ($this->Intervals->save($interval)) {
                $this->Flash->success(__('O intervalo foi adicionado com sucesso.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('O intervalo não foi adicionado.Por favor, tente novamente.'));
        }
        $companies = $this->Intervals->Vehicles->Companies->find('list', ['limit' => 200]);
        $this->set(compact('interval', 'companies'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Interval id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $interval = $this->Intervals->get($id, [
            'contain' => ['Vehicles'=>'Companies']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $interval = $this->Intervals->patchEntity($interval, $this->request->getData());
            if ($this->Intervals->save($interval)) {
                $this->Flash->success(__('O intervalo foi atualizado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('O intervalo não foi atualizado. Por favor, tente novamente.'));
        }
        $companies = $this->Intervals->Vehicles->Companies->find('list', ['limit' => 200]);
        $companyIdSelected = $interval->vehicle->company->id;
        $vehicleIdSelected = $interval->vehicle_id;
        $this->set(compact('interval', 'companies','companyIdSelected','vehicleIdSelected'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Interval id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $interval = $this->Intervals->get($id);
        if ($this->Intervals->delete($interval)) {
            $this->Flash->success(__('O intervalo foi excluído com sucesso.'));
        } else {
            $this->Flash->error(__('O intervalo não foi excluído. Por favor, tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
