<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * QueryVehicleTypes Controller
 *
 * @property \App\Model\Table\QueryVehicleTypesTable $QueryVehicleTypes
 *
 * @method \App\Model\Entity\QueryVehicleType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class QueryVehicleTypesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Queries', 'VehicleTypes']
        ];
        $queryVehicleTypes = $this->paginate($this->QueryVehicleTypes);

        $this->set(compact('queryVehicleTypes'));
    }

    /**
     * View method
     *
     * @param string|null $id Query Vehicle Type id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $queryVehicleType = $this->QueryVehicleTypes->get($id, [
            'contain' => ['Queries', 'VehicleTypes']
        ]);

        $this->set('queryVehicleType', $queryVehicleType);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $queryVehicleType = $this->QueryVehicleTypes->newEntity();
        if ($this->request->is('post')) {
            $queryVehicleType = $this->QueryVehicleTypes->patchEntity($queryVehicleType, $this->request->getData());
            if ($this->QueryVehicleTypes->save($queryVehicleType)) {
                $this->Flash->success(__('The query vehicle type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The query vehicle type could not be saved. Please, try again.'));
        }
        $queries = $this->QueryVehicleTypes->Queries->find('list', ['limit' => 200]);
        $vehicleTypes = $this->QueryVehicleTypes->VehicleTypes->find('list', ['limit' => 200]);
        $this->set(compact('queryVehicleType', 'queries', 'vehicleTypes'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Query Vehicle Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $queryVehicleType = $this->QueryVehicleTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $queryVehicleType = $this->QueryVehicleTypes->patchEntity($queryVehicleType, $this->request->getData());
            if ($this->QueryVehicleTypes->save($queryVehicleType)) {
                $this->Flash->success(__('The query vehicle type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The query vehicle type could not be saved. Please, try again.'));
        }
        $queries = $this->QueryVehicleTypes->Queries->find('list', ['limit' => 200]);
        $vehicleTypes = $this->QueryVehicleTypes->VehicleTypes->find('list', ['limit' => 200]);
        $this->set(compact('queryVehicleType', 'queries', 'vehicleTypes'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Query Vehicle Type id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $queryVehicleType = $this->QueryVehicleTypes->get($id);
        if ($this->QueryVehicleTypes->delete($queryVehicleType)) {
            $this->Flash->success(__('The query vehicle type has been deleted.'));
        } else {
            $this->Flash->error(__('The query vehicle type could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
