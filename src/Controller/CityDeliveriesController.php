<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CityDeliveries Controller
 *
 * @property \App\Model\Table\CityDeliveriesTable $CityDeliveries
 *
 * @method \App\Model\Entity\CityDelivery[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CityDeliveriesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Cities', 'Companies']
        ];
        $cityDeliveries = $this->paginate($this->CityDeliveries);

        $this->set(compact('cityDeliveries'));
    }

    /**
     * View method
     *
     * @param string|null $id City Delivery id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $cityDelivery = $this->CityDeliveries->get($id, [
            'contain' => ['Cities'=>'States', 'Companies']
        ]);

        $this->set('cityDelivery', $cityDelivery);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($company_id = null)
    {
        $cityDelivery = $this->CityDeliveries->newEntity();
        if ($this->request->is('post')) {
            $cityDelivery = $this->CityDeliveries->patchEntity($cityDelivery, $this->request->getData());
            if ($this->CityDeliveries->save($cityDelivery)) {
                $this->Flash->success(__('Cidade para entrega cadastrada com sucesso.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Cidade para entrega não foi salvo. Por favor, tente novamente.'));
        }
        if($company_id != null){
            $company = $this->CityDeliveries->Companies->find('all')->where(['Companies.id ' => $company_id])->first();   
            $this->set('company',$company);
        }else{
            $companies = $this->CityDeliveries->Companies->find('list', ['limit' => 200]);
            $this->set('companies',$companies);
        }
        $cityIdSelected = $this->request->getData('city_id');
        $this->loadModel('States');
        $states = $this->States->find('list');
        $this->set(compact('cityDelivery','states','cityIdSelected'));
    }

    /**
     * Edit method
     *
     * @param string|null $id City Delivery id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cityDelivery = $this->CityDeliveries->get($id, [
            'contain' => ['Cities','Companies']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cityDelivery = $this->CityDeliveries->patchEntity($cityDelivery, $this->request->getData());
            if ($this->CityDeliveries->save($cityDelivery)) {
                $this->Flash->success(__('Cidade para entrega salva com sucesso.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('A cidade para entrega não foi atualizada. Por favor, tente novamente.'));
        }
        $this->loadModel('States');
        $states = $this->States->find('list');
        $this->set('stateIdSelected', $cityDelivery->city->state_id);
        $this->set('cityIdSelected', $cityDelivery->city_id);

        $this->set(compact('cityDelivery','states'));
    }

    /**
     * Delete method
     *
     * @param string|null $id City Delivery id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $cityDelivery = $this->CityDeliveries->get($id);
        if ($this->CityDeliveries->delete($cityDelivery)) {
            $this->Flash->success(__('A cidade para entrega foi excluída.'));
        } else {
            $this->Flash->error(__('A cidade para entrega não foi excluída. Por favor, tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
