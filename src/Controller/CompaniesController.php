<?php
// src/Controller/CompaniesController.php

namespace App\Controller;

use App\Controller\AppController;

class CompaniesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->loadComponent('Flash'); // Include the FlashComponent
    }

    public function index()
    {
        $companies = $this->Paginator->paginate($this->Companies->find());
        $this->set(compact('companies'));
    }

    public function view($id)
    {
        $company = $this->Companies->get($id, [
            'contain' => ['Cities'=>'States']
        ]);
        $this->set(compact('company'));
    }

    public function add()
    {
        $company = $this->Companies->newEntity();
        if ($this->request->is('post')) {            
            $company = $this->Companies->patchEntity($company, $this->request->getData());
            
            if ($this->Companies->save($company)) {
                $this->Flash->success(__('Empresa salva com sucesso.'));
                return $this->redirect(['action' => 'index']);

            }else{
                $this->Flash->error(__('Falha ao adicionar empresa.'));
            }
        }
        $this->loadModel('States');
        $states = $this->States->find('list');
        $cityIdSelected = $this->request->getData('city_id');
        $this->set(compact('company','states','cityIdSelected'));
        $this->set('requiredLogo',true);
    }

    public function edit($id)
    {
        $company = $this->Companies->get($id, [
            'contain' => ['Cities']
        ]);
        if ($this->request->is(['post', 'put'])) {

            $this->Companies->patchEntity($company, $this->request->getData());
            if ($this->Companies->save($company)) {
                $this->Flash->success(__('Empresa atualizada com sucesso.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Falha ao atualizar a empresa.'));
        }
        $this->loadModel('States');

        $this->set('stateIdSelected', $company->city->state_id);
        $this->set('cityIdSelected', $company->city_id);
        $states = $this->States->find('list',['fields'=>['id','uf']]);
        $this->set(compact('company','states'));
        $this->set('requiredLogo',false);
    }

    public function delete($id)
    {
        $this->request->allowMethod(['post', 'delete']);

        $company = $this->Companies->findById($id)->firstOrFail();
        
        try {
            if ($this->Companies->delete($company)) {
                $this->Flash->success(__('A empresa {0} foi excluída.', $company->name));
            }
        } catch (\PDOException $e) {
            $this->Flash->error(__('A empresa que você está tentando excluir tem dados associados a ela, favor verificar e tentar novamente.'));
        }   
        return $this->redirect(['action' => 'index']);
    }

}