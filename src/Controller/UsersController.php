<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Core\Configure;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Roles', 'Cities']
        ];
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Roles', 'Cities']
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());

            if ($this->Users->save($user)) {
                $this->Flash->success(__('Usuário salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            }else{
                $this->Flash->error(__('Usuário não foi salvo. Por favor tente novamente.'));
            }
        }
        $this->loadModel('States');
        $publicRole = Configure::read('App.publicRole');
        $states = $this->States->find('list');
        $cityIdSelected = $this->request->getData('city_id');
        $this->set(compact('user', 'publicRole', 'states','cityIdSelected'));
    }

    public function addAdmin()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());

            if ($this->Users->save($user)) {
                $this->Flash->success(__('Usuário salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            }else{
                $this->Flash->error(__('Usuário não foi salvo. Por favor tente novamente.'));
            }
        }
        $this->loadModel('States');
        $states = $this->States->find('list');
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $cityIdSelected = $this->request->getData('city_id');
        $this->set(compact('user', 'roles', 'states','cityIdSelected'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Cities']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Usuário salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Usuário não pode ser salvo. Por favor, tente novamente.'));
        }
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->loadModel('States');
        $this->set('stateIdSelected', $user->city->state_id);
        $this->set('cityIdSelected', $user->city_id);
        $states = $this->States->find('list');
        $this->set(compact('user','states','roles'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('Usuário excuído.'));
        } else {
            $this->Flash->error(__('Usuário não pode ser excluído. Por favor, tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function login(){
        if($this->request->is('post')){
            $user = $this->Auth->identify();
            if($user){
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Usuário ou senha inválidos, tente novamente'));
        }
    }

    public function logout(){
        return $this->redirect($this->Auth->logout());
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->set('email',$this->Auth->user('email'));
        $this->Auth->allow('add');
    }

    public function changePassword($id=null)
    {
        $user = $this->Users->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if (!empty($this->request->data)) {
                $user = $this->Users->patchEntity($user, [
                        'current_password'  => $this->request->data['current_password'],
                        'password'      => $this->request->data['password1'],
                        'password1'     => $this->request->data['password1'],
                        'password2'     => $this->request->data['password2']
                    ],
                    ['validate' => 'password']
                );
                if ($this->Users->save($user)) {
                    $this->Flash->success('Senha alterada com sucesso.');
                    $this->redirect(['action' => 'index']);
                } else {
                    if($user->errors()){
                        $error_msg = [];
                        foreach( $user->errors() as $errors){
                            if(is_array($errors)){
                                foreach($errors as $error){
                                    $error_msg[]    =   $error;
                                }
                            }else{
                                $error_msg[]    =   $errors;
                            }
                        }

                        if(!empty($error_msg)){
                            $this->Flash->error(
                                __("Por favor atente ao(s) seguinte(s) erro(s):".implode("\n \r ", $error_msg))
                            );
                        }
                    }
                }
            }
        }
        $this->set('user',$user);
    }

    public function listInactives(){
        if ($this->request->is(['patch', 'post', 'put'])) {
            $datas = $this->request->getData('is_active');
            $saved = array();
            $savedFail = array();
            for($i=0; $i<sizeof($datas);$i++){
                if($datas[$i]!=0){
                    $user = $this->Users->get($datas[$i]);
                    $user->is_active = 1;
                    if ($this->Users->save($user)) {
                        array_push($saved,$user->name);
                    }else{
                        array_push($savedFail,$user->name);
                    }
                }
            }
            if(!empty($saved)){
                $this->Flash->success('Ativado com sucesso o(s) usuário(s): '.implode(", ", $saved));
            }
            if (!empty($savedFail)) {
                $this->Flash->error('Falha ao ativar o(s) usuário(s): '.implode(", ", $savedFail));   
            }
            return $this->redirect(['action' => 'list-inactives']);
        }
        $users = $this->Users->find('all')->where(['is_active'=>'0']);
        $this->set('users',$users);
    }
}
