<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * VehicleTypes Controller
 *
 * @property \App\Model\Table\VehicleTypesTable $VehicleTypes
 *
 * @method \App\Model\Entity\VehicleType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class VehicleTypesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $vehicleTypes = $this->paginate($this->VehicleTypes);

        $this->set(compact('vehicleTypes'));
    }

    /**
     * View method
     *
     * @param string|null $id Vehicle Type id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $vehicleType = $this->VehicleTypes->get($id, [
            'contain' => ['Vehicles'=>['Companies', 'VehicleTypes']]
        ]);

        $this->set('vehicleType', $vehicleType);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $vehicleType = $this->VehicleTypes->newEntity();
        if ($this->request->is('post')) {
            $vehicleType = $this->VehicleTypes->patchEntity($vehicleType, $this->request->getData());
            if ($this->VehicleTypes->save($vehicleType)) {
                $this->Flash->success(__('The vehicle type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The vehicle type could not be saved. Please, try again.'));
        }
        $this->set(compact('vehicleType'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Vehicle Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $vehicleType = $this->VehicleTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $vehicleType = $this->VehicleTypes->patchEntity($vehicleType, $this->request->getData());
            if ($this->VehicleTypes->save($vehicleType)) {
                $this->Flash->success(__('The vehicle type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The vehicle type could not be saved. Please, try again.'));
        }
        $this->set(compact('vehicleType'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Vehicle Type id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $vehicleType = $this->VehicleTypes->get($id);
        if ($this->VehicleTypes->delete($vehicleType)) {
            $this->Flash->success(__('The vehicle type has been deleted.'));
        } else {
            $this->Flash->error(__('The vehicle type could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
