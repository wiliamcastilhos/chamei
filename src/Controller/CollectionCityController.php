<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CollectionCity Controller
 *
 * @property \App\Model\Table\CollectionCityTable $CollectionCity
 *
 * @method \App\Model\Entity\CollectionCity[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CollectionCityController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Cities', 'Companies']
        ];
        $collectionCity = $this->paginate($this->CollectionCity);

        $this->set(compact('collectionCity'));
    }

    /**
     * View method
     *
     * @param string|null $id Collection City id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $collectionCity = $this->CollectionCity->get($id, [
            'contain' => ['Cities', 'Companies']
        ]);

        $this->set('collectionCity', $collectionCity);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $collectionCity = $this->CollectionCity->newEntity();
        if ($this->request->is('post')) {
            $collectionCity = $this->CollectionCity->patchEntity($collectionCity, $this->request->getData());
            if ($this->CollectionCity->save($collectionCity)) {
                $this->Flash->success(__('The collection city has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The collection city could not be saved. Please, try again.'));
        }
        $cities = $this->CollectionCity->Cities->find('list', ['limit' => 200]);
        $companies = $this->CollectionCity->Companies->find('list', ['limit' => 200]);
        $this->set(compact('collectionCity', 'cities', 'companies'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Collection City id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $collectionCity = $this->CollectionCity->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $collectionCity = $this->CollectionCity->patchEntity($collectionCity, $this->request->getData());
            if ($this->CollectionCity->save($collectionCity)) {
                $this->Flash->success(__('The collection city has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The collection city could not be saved. Please, try again.'));
        }
        $cities = $this->CollectionCity->Cities->find('list', ['limit' => 200]);
        $companies = $this->CollectionCity->Companies->find('list', ['limit' => 200]);
        $this->set(compact('collectionCity', 'cities', 'companies'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Collection City id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $collectionCity = $this->CollectionCity->get($id);
        if ($this->CollectionCity->delete($collectionCity)) {
            $this->Flash->success(__('The collection city has been deleted.'));
        } else {
            $this->Flash->error(__('The collection city could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
