<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * QuerySolicitations Controller
 *
 * @property \App\Model\Table\QuerySolicitationsTable $QuerySolicitations
 *
 * @method \App\Model\Entity\QuerySolicitation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class QuerySolicitationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Companies', 'Queries']
        ];
        $querySolicitations = $this->paginate($this->QuerySolicitations);

        $this->set(compact('querySolicitations'));
    }

    /**
     * View method
     *
     * @param string|null $id Query Solicitation id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $querySolicitation = $this->QuerySolicitations->get($id, [
            'contain' => ['Users', 'Companies', 'Queries']
        ]);

        $this->set('querySolicitation', $querySolicitation);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $querySolicitation = $this->QuerySolicitations->newEntity();
        if ($this->request->is('post')) {
            $querySolicitation = $this->QuerySolicitations->patchEntity($querySolicitation, $this->request->getData());
            if ($this->QuerySolicitations->save($querySolicitation)) {
                $this->Flash->success(__('The query solicitation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The query solicitation could not be saved. Please, try again.'));
        }
        $users = $this->QuerySolicitations->Users->find('list', ['limit' => 200]);
        $companies = $this->QuerySolicitations->Companies->find('list', ['limit' => 200]);
        $queries = $this->QuerySolicitations->Queries->find('list', ['limit' => 200]);
        $this->set(compact('querySolicitation', 'users', 'companies', 'queries'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Query Solicitation id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $querySolicitation = $this->QuerySolicitations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $querySolicitation = $this->QuerySolicitations->patchEntity($querySolicitation, $this->request->getData());
            if ($this->QuerySolicitations->save($querySolicitation)) {
                $this->Flash->success(__('The query solicitation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The query solicitation could not be saved. Please, try again.'));
        }
        $users = $this->QuerySolicitations->Users->find('list', ['limit' => 200]);
        $companies = $this->QuerySolicitations->Companies->find('list', ['limit' => 200]);
        $queries = $this->QuerySolicitations->Queries->find('list', ['limit' => 200]);
        $this->set(compact('querySolicitation', 'users', 'companies', 'queries'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Query Solicitation id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $querySolicitation = $this->QuerySolicitations->get($id);
        if ($this->QuerySolicitations->delete($querySolicitation)) {
            $this->Flash->success(__('The query solicitation has been deleted.'));
        } else {
            $this->Flash->error(__('The query solicitation could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function budget(){
        $this -> autoRender = false;   
        if ($this->request->is('ajax')) {
            $querySolicitation = $this->QuerySolicitations->patchEntity($this->QuerySolicitations->newEntity(), $this->request->getData());
            // if ($this->QuerySolicitations->save($querySolicitation)) {
            debug($querySolicitation);
            // $result = json_encode(['status'=>'OK']);
            // $this->response->type('json');
            // $this->response->body($result);
            // return $this->response;
        }        
    }

    private function saveSolicitation(){
        
    }
}
