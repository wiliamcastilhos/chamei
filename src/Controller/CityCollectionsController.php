<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CityCollections Controller
 *
 * @property \App\Model\Table\CityCollectionsTable $CityCollections
 *
 * @method \App\Model\Entity\CityCollection[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CityCollectionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Cities', 'Companies']
        ];
        $cityCollections = $this->paginate($this->CityCollections);

        $this->set(compact('cityCollections'));
    }

    /**
     * View method
     *
     * @param string|null $id City Collection id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $cityCollection = $this->CityCollections->get($id, [
            'contain' => ['Cities'=>'States', 'Companies']
        ]);

        $this->set('cityCollection', $cityCollection);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($company_id = null)
    {
        $cityCollection = $this->CityCollections->newEntity();
        if ($this->request->is('post')) {
            $cityCollection = $this->CityCollections->patchEntity($cityCollection, $this->request->getData());
            if ($this->CityCollections->save($cityCollection)) {
                $this->Flash->success(__('Cidade para coleta cadastrada com sucesso.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Cidade para coleta não foi salvo. Por favor, tente novamente.'));
        }
        if($company_id != null){
            $company = $this->CityCollections->Companies->find('all')->where(['Companies.id ' => $company_id])->first();   
            $this->set('company',$company);
        }else{
            $companies = $this->CityCollections->Companies->find('list', ['limit' => 200]);
            $this->set('companies',$companies);
        }
        $cityIdSelected = $this->request->getData('city_id');
        $this->loadModel('States');
        $states = $this->States->find('list');
        $this->set(compact('cityCollection','states','cityIdSelected'));
    }

    /**
     * Edit method
     *
     * @param string|null $id City Collection id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cityCollection = $this->CityCollections->get($id, [
            'contain' => ['Cities','Companies']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cityCollection = $this->CityCollections->patchEntity($cityCollection, $this->request->getData());
            if ($this->CityCollections->save($cityCollection)) {
                $this->Flash->success(__('Cidade para coleta salva com sucesso.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('A cidade para coleta não foi atualizada. Por favor, tente novamente.'));
        }
        $this->loadModel('States');
        $states = $this->States->find('list');
        $this->set('stateIdSelected', $cityCollection->city->state_id);
        $this->set('cityIdSelected', $cityCollection->city_id);

        $this->set(compact('cityCollection','states'));
    }

    /**
     * Delete method
     *
     * @param string|null $id City Collection id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $cityCollection = $this->CityCollections->get($id);
        if ($this->CityCollections->delete($cityCollection)) {
            $this->Flash->success(__('A cidade para coleta foi excluída.'));
        } else {
            $this->Flash->error(__('A cidade para coleta não foi excluída. Por favor, tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
