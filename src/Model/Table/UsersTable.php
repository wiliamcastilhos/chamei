<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Users Model
 *
 * @property \App\Model\Table\RolesTable|\Cake\ORM\Association\BelongsTo $Roles
 * @property \App\Model\Table\CitiesTable|\Cake\ORM\Association\BelongsTo $Cities
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Roles', [
            'foreignKey' => 'roles_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Cities', [
            'foreignKey' => 'city_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 50)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email')
            ->add('email', 'unique', ['rule' => 'validateUnique', 'provider' => 'table','message'=>'E-mail informado já está em uso.']);

        $validator
            ->scalar('cep')
            ->maxLength('cep', 8)
            ->allowEmpty('cep');

        $validator
            ->scalar('address')
            ->maxLength('address', 80)
            ->requirePresence('address', 'create')
            ->notEmpty('address');

        $validator
            ->scalar('street_number')
            ->maxLength('street_number', 5)
            ->requirePresence('street_number', 'create')
            ->notEmpty('street_number');

        $validator
            ->scalar('complement')
            ->maxLength('complement', 50)
            ->allowEmpty('complement');

        $validator
            ->scalar('neighborhood')
            ->maxLength('neighborhood', 50)
            ->requirePresence('neighborhood', 'create')
            ->notEmpty('neighborhood');

        $validator
            ->scalar('contact_phone')
            ->maxLength('contact_phone', 10)
            ->allowEmpty('contact_phone');

        $validator
            ->scalar('cell_phone')
            ->maxLength('cell_phone', 11)
            ->allowEmpty('cell_phone');

        $validator
            ->scalar('password')
            ->maxLength('password', 12)
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->add(
                'confirm_password',
                'compareWith',[
                    'rule' => ['compareWith', 'password'],
                    'message' => 'Senhas não conferem.'
                ]
            );

        $validator
            ->boolean('is_active')
            ->requirePresence('is_active', 'create')
            ->notEmpty('is_active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['roles_id'], 'Roles'));
        $rules->add($rules->existsIn(['city_id'], 'Cities'));

        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        if (isset($data['contact_phone'])) {
            $data['contact_phone'] = preg_replace('/\D/', '', $data['contact_phone']);
        }

        if (isset($data['cell_phone'])) {
            $data['cell_phone'] = preg_replace('/\D/', '', $data['cell_phone']);
        }

        if (isset($data['cep'])) {
            $data['cep'] = preg_replace('/\D/', '', $data['cep']);
        }
    }

    public function validationPassword(Validator $validator )
    {

        $validator
            ->add('current_password','custom',[
                'rule'=>  function($value, $context){
                    $user = $this->get($context['data']['id']);
                    if ($user) {
                        if ((new DefaultPasswordHasher)->check($value, $user->password)) {
                            return true;
                        }
                    }
                    return false;
                },
                'message'=>'Senha informada está incorreta.',
            ])
            ->notEmpty('current_password');
        $validator
            ->add('password2',[
                'match'=>[
                    'rule'=> ['compareWith','password1'],
                    'message'=>'Senhas não conferem.',
                ]
            ])
            ->notEmpty('password2');

        return $validator;
    }
}
