<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;

/**
 * Companies Model
 *
 * @property \App\Model\Table\CitiesTable|\Cake\ORM\Association\BelongsTo $Cities
 *
 * @method \App\Model\Entity\Company get($primaryKey, $options = [])
 * @method \App\Model\Entity\Company newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Company[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Company|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Company patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Company[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Company findOrCreate($search, callable $callback = null, $options = [])
 */
class CompaniesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->addBehavior('Timestamp');
        parent::initialize($config);
        $this->addBehavior('Sluggable');

        $this->setTable('companies');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Cities', [
            'foreignKey' => 'city_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('Calendars', [
            'foreignKey' => 'company_id'
        ]);
        $this->hasMany('CityCollections', [
            'foreignKey' => 'company_id'
        ]);
        $this->hasMany('CityDeliveries', [
            'foreignKey' => 'company_id'
        ]);
        $this->hasMany('QueryHistories', [
            'foreignKey' => 'company_id'
        ]);
        $this->hasMany('Vehicles', [
            'foreignKey' => 'company_id'
        ]);

        $this->addBehavior('Proffer.Proffer', [
            'logo' => [
                'root' => WWW_ROOT . 'uploads',
                'dir' => 'logo_dir',   
                'thumbnailSizes' => [ 
                    'square' => [   
                        'w' => 200, 
                        'h' => 200, 
                        'jpeg_quality'  => 100
                    ],
                    'portrait' => [
                        'w' => 100,
                        'h' => 300
                    ],
                ],
                'thumbnailMethod' => 'gd'
            ]
        ]);
    }

    public function beforeSave($event, $entity, $options)
    {
       if ($entity->isNew() && !$entity->slug) {
           $sluggedTitle = $this->slug($entity);
           // trim slug to maximum length defined in schema
           // $entity->slug = substr($sluggedTitle, 0, 200);
       }
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 50)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('cnpj')
            ->maxLength('cnpj', 14)
            ->notEmpty('cnpj');

        $validator
            ->email('email','valid-email', ['rule' => 'email'])
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->scalar('cep')
            ->maxLength('cep', 50)
            ->allowEmpty('cep');

        $validator
            ->scalar('address')
            ->maxLength('address', 80)
            ->requirePresence('address', 'create')
            ->notEmpty('address');

        $validator
            ->scalar('street_number')
            ->requirePresence('street_number', 'create')
            ->notEmpty('street_number');

        $validator
            ->integer('city_id')
            ->requirePresence('city_id', 'create')
            ->notEmpty('city_id');

        $validator
            ->scalar('complement')
            ->maxLength('complement', 50)
            ->allowEmpty('complement');

        $validator
            ->scalar('neighborhood')
            ->maxLength('neighborhood', 50)
            ->requirePresence('neighborhood', 'create')
            ->notEmpty('neighborhood');

        $validator
            ->scalar('contact_phone')
            ->maxLength('contact_phone', 10)
            ->allowEmpty('contact_phone');

        $validator
            ->scalar('cell_phone')
            ->maxLength('cell_phone', 11)
            ->allowEmpty('cell_phone');

        $validator
            ->time('initial_morning_hour')
            ->allowEmpty('initial_morning_hour');

        $validator
            ->time('final_morning_hour')
            ->allowEmpty('final_morning_hour');

        $validator
            ->time('initial_afternoon_hour')
            ->allowEmpty('initial_afternoon_hour');

        $validator
            ->time('final_afternoon_hour')
            ->allowEmpty('final_afternoon_hour');

        $validator
        ->add('logo', [
                    'uploadError' => [
                        'rule' => 'uploadError',
                        'message' => 'Falha ao fazer upload do logo.',
                        'last' => true
                    ],
                    'fileSize' => [
                            'rule' => [
                                'fileSize', '<=', '5MB'
                            ],
                            'message' => 'Por favor envie arquivos de até 5MB.'
                        ],
                    'mimeType' => [
                            'rule' => [
                                'mimeType', ['image/jpeg','image/png','image/jpg']
                            ],
                            'message' => 'Por favor envie apenas imagens.'
                    ]
                ]
        )
        ->requirePresence('logo', 'create')
        ->allowEmpty('logo',function ($context) {
                        return empty($context['data']['requiredLogo']);
                    });

        // $validator
        //     ->requirePresence('is_active', 'create')
        //     ->notEmpty('is_active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->isUnique(['email']));
        // $rules->add($rules->existsIn(['city_id'], 'Cities'));
        // $rules->addDelete(function ($entity, $options) {
        //     debug($options);
        //     return false;
        // }, 'deleteRule');

        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        if (isset($data['cnpj'])) {
            $data['cnpj'] = preg_replace('/\D/', '', $data['cnpj']);
        }

        if (isset($data['contact_phone'])) {
            $data['contact_phone'] = preg_replace('/\D/', '', $data['contact_phone']);
        }

        if (isset($data['cell_phone'])) {
            $data['cell_phone'] = preg_replace('/\D/', '', $data['cell_phone']);
        }

        if (isset($data['cep'])) {
            $data['cep'] = preg_replace('/\D/', '', $data['cep']);
        }
    }
    
}
