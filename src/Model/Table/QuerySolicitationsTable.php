<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;

/**
 * QuerySolicitations Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\CompaniesTable|\Cake\ORM\Association\BelongsTo $Companies
 * @property \App\Model\Table\QueriesTable|\Cake\ORM\Association\BelongsTo $Queries
 *
 * @method \App\Model\Entity\QuerySolicitation get($primaryKey, $options = [])
 * @method \App\Model\Entity\QuerySolicitation newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\QuerySolicitation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\QuerySolicitation|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\QuerySolicitation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\QuerySolicitation[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\QuerySolicitation findOrCreate($search, callable $callback = null, $options = [])
 */
class QuerySolicitationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('query_solicitations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Queries', [
            'foreignKey' => 'query_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->date('date')
            ->requirePresence('date', 'create')
            ->notEmpty('date');

        $validator
            ->decimal('normal_value')
            ->requirePresence('normal_value', 'create')
            ->notEmpty('normal_value');

        $validator
            ->decimal('express_value')
            ->requirePresence('express_value', 'create')
            ->notEmpty('express_value');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['company_id'], 'Companies'));
        $rules->add($rules->existsIn(['query_id'], 'Queries'));

        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options) {
        debug($data);
        if (!isset($data['date'])) {
            $data['date'] = Time::now();
        }
        if (!isset($data['user_id'])) {
            $data['user_id'] = $this->Auth->user()->id;
        }
        if (!isset($data['company_id'])) {
            $data['company_id'] = $data[''];
        }
        if (!isset($data['date'])) {
            $data['date'] = Time::now();
        }
        if (!isset($data['date'])) {
            $data['date'] = Time::now();
        }
        if (!isset($data['date'])) {
            $data['date'] = Time::now();
        }
        if (!isset($data['date'])) {
            $data['date'] = Time::now();
        }
    }
}
