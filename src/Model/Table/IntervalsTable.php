<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Intervals Model
 *
 * @property \App\Model\Table\VehiclesTable|\Cake\ORM\Association\BelongsTo $Vehicles
 *
 * @method \App\Model\Entity\Interval get($primaryKey, $options = [])
 * @method \App\Model\Entity\Interval newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Interval[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Interval|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Interval patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Interval[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Interval findOrCreate($search, callable $callback = null, $options = [])
 */
class IntervalsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('intervals');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Vehicles', [
            'foreignKey' => 'vehicle_id',
            'joinType' => 'INNER',
            'dependent' => true
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('distance_from')
            ->requirePresence('distance_from', 'create')
            ->notEmpty('distance_from');

        $validator
            ->integer('distance_to')
            ->requirePresence('distance_to', 'create')
            ->notEmpty('distance_to');

        $validator
            ->decimal('normal_value')
            ->requirePresence('normal_value', 'create')
            ->notEmpty('normal_value');

        $validator
            ->decimal('express_value')
            ->requirePresence('express_value', 'create')
            ->notEmpty('express_value');

        $validator
            ->integer('vehicle_id')
            ->requirePresence('vehicle_id', 'create')
            ->notEmpty('vehicle_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['vehicle_id'], 'Vehicles'));

        return $rules;
    }
}
