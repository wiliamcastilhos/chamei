<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * QueryVehicleTypes Model
 *
 * @property \App\Model\Table\QueriesTable|\Cake\ORM\Association\BelongsTo $Queries
 * @property \App\Model\Table\VehicleTypesTable|\Cake\ORM\Association\BelongsTo $VehicleTypes
 *
 * @method \App\Model\Entity\QueryVehicleType get($primaryKey, $options = [])
 * @method \App\Model\Entity\QueryVehicleType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\QueryVehicleType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\QueryVehicleType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\QueryVehicleType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\QueryVehicleType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\QueryVehicleType findOrCreate($search, callable $callback = null, $options = [])
 */
class QueryVehicleTypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('query_vehicle_types');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Queries', [
            'foreignKey' => 'query_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('VehicleTypes', [
            'foreignKey' => 'vehicle_type_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['query_id'], 'Queries'));
        $rules->add($rules->existsIn(['vehicle_type_id'], 'VehicleTypes'));

        return $rules;
    }
}
