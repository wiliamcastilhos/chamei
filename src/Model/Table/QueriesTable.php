<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;
use Cake\I18n\Time;

/**
 * Queries Model
 *
 * @property \App\Model\Table\CityCollectionsTable|\Cake\ORM\Association\BelongsTo $CityCollections
 * @property \App\Model\Table\CityDeliveriesTable|\Cake\ORM\Association\BelongsTo $CityDeliveries
 * @property \App\Model\Table\QuerySolicitationsTable|\Cake\ORM\Association\HasMany $QuerySolicitations
 *
 * @method \App\Model\Entity\Query get($primaryKey, $options = [])
 * @method \App\Model\Entity\Query newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Query[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Query|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Query patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Query[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Query findOrCreate($search, callable $callback = null, $options = [])
 */
class QueriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('queries');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Cities', [
            'foreignKey' => 'city_collection_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Cities', [
            'foreignKey' => 'city_delivery_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('QuerySolicitations', [
            'foreignKey' => 'query_id'
        ]);

        $this->hasMany('QueryVehicleTypes', [
            'foreignKey' => 'query_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->date('date')
            ->requirePresence('date', 'create')
            ->notEmpty('date');

        $validator
            ->scalar('collection_address')
            ->maxLength('collection_address', 50)
            ->requirePresence('collection_address', 'create')
            ->notEmpty('collection_address');

        $validator
            ->scalar('collection_street_number')
            ->maxLength('collection_street_number', 5)
            ->requirePresence('collection_street_number', 'create')
            ->notEmpty('collection_street_number');

        $validator
            ->scalar('collection_neighborhood')
            ->maxLength('collection_neighborhood', 50)
            ->allowEmpty('collection_neighborhood');

        $validator
            ->scalar('collection_cep')
            ->allowEmpty('collection_cep');

        $validator
            ->scalar('collection_complement')
            ->maxLength('collection_complement', 20)
            ->allowEmpty('collection_complement');

        $validator
            ->scalar('collection_references')
            ->maxLength('collection_references', 50)
            ->allowEmpty('collection_references');

        $validator
            ->scalar('delivery_address')
            ->maxLength('delivery_address', 50)
            ->requirePresence('delivery_address', 'create')
            ->notEmpty('delivery_address');

        $validator
            ->scalar('delivery_street_number')
            ->maxLength('delivery_street_number', 5)
            ->requirePresence('delivery_street_number', 'create')
            ->notEmpty('delivery_street_number');

        $validator
            ->scalar('delivery_neighborhood')
            ->maxLength('delivery_neighborhood', 50)
            ->allowEmpty('delivery_neighborhood');

        $validator
            ->scalar('delivery_cep')
            ->allowEmpty('delivery_cep');

        $validator
            ->scalar('delivery_complement')
            ->maxLength('delivery_complement', 20)
            ->allowEmpty('delivery_complement');

        $validator
            ->scalar('delivery_references')
            ->maxLength('delivery_references', 50)
            ->allowEmpty('delivery_references');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['city_collection_id'], 'Cities'));
        $rules->add($rules->existsIn(['city_delivery_id'], 'Cities'));

        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options) {
        if(isset($data['delivery_cep'])){
            $data['delivery_cep'] = preg_replace('/\D/', '', $data['delivery_cep']);
        }
        if(isset($data['collection_cep'])){
            $data['collection_cep'] = preg_replace('/\D/', '', $data['collection_cep']);
        }
        if (!isset($data['date'])) {
            $data['date'] = Time::now();
        }
    }
}
