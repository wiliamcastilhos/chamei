<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;

/**
 * Calendars Model
 *
 * @property \App\Model\Table\CompaniesTable|\Cake\ORM\Association\BelongsTo $Companies
 *
 * @method \App\Model\Entity\Calendar get($primaryKey, $options = [])
 * @method \App\Model\Entity\Calendar newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Calendar[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Calendar|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Calendar patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Calendar[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Calendar findOrCreate($search, callable $callback = null, $options = [])
 */
class CalendarsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('calendars');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id',
            'joinType' => 'INNER'
        ]);

        $this->addBehavior('Search.Search');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('year')
            ->requirePresence('year', 'create')
            ->notEmpty('year');
            // ->add('year', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->date('date')
            ->requirePresence('date', 'create')
            ->notEmpty('date');

        $validator
            ->time('initial_morning_hour')
            ->requirePresence('initial_morning_hour', 'create')
            ->notEmpty('initial_morning_hour');

        $validator
            ->time('final_morning_hour')
            ->requirePresence('final_morning_hour', 'create')
            ->notEmpty('final_morning_hour');

        $validator
            ->time('initial_afternoon_hour')
            ->requirePresence('initial_afternoon_hour', 'create')
            ->notEmpty('initial_afternoon_hour');

        $validator
            ->time('final_afternoon_hour')
            ->requirePresence('final_afternoon_hour', 'create')
            ->notEmpty('final_afternoon_hour');

        $validator
            ->boolean('is_util_day')
            ->requirePresence('is_util_day', 'create')
            ->notEmpty('is_util_day');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->isUnique(['year']));
        $rules->add($rules->existsIn(['company_id'], 'Companies'));

        return $rules;
    }

    public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();
        $searchManager
            ->value('year')
            ->value('company_id')
            ->value('date');//date('Y-m-d', strtotime($data['date']))
            // ->finder('calendar');
            // ->add('save_query', 'Search.Callback', [
            //         'callback' => function ($query, $args, $filter) {
            //             // debug(':-(');
            //             // debug($query);
            //             // debug($args);
            //             // debug($filter);
            //             // $this->redirect('action','saveQuery');
            //         }
            //     ]);
        // ->add('busca',;

        return $searchManager;
    }

    // public function findCalendar(Query $query, array $options)
    // {
    //     debug($options);
    //     $calendar = $options['calendar'];
    //     return $query->where(function (QueryExpression $exp){
    //                 $orConditions = $exp->or_(function ($or) 
    //                 {
    //                     return $or->eq('year', $calendar['year'])
    //                         ->eq('company_id', $calendar['company_id'])
    //                         ->eq('date', date('Y-m-d', strtotime($calendar['date'])));
    //                 });
    //                 return $exp
    //                     ->not($orConditions);
    //             });
    // }

}
