<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Vehicles Model
 *
 * @property \App\Model\Table\CompaniesTable|\Cake\ORM\Association\BelongsTo $Companies
 * @property \App\Model\Table\VehicleTypesTable|\Cake\ORM\Association\BelongsTo $VehicleTypes
 * @property \App\Model\Table\IntervalsTable|\Cake\ORM\Association\HasMany $Intervals
 *
 * @method \App\Model\Entity\Vehicle get($primaryKey, $options = [])
 * @method \App\Model\Entity\Vehicle newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Vehicle[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Vehicle|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Vehicle patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Vehicle[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Vehicle findOrCreate($search, callable $callback = null, $options = [])
 */
class VehiclesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('vehicles');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('VehicleTypes', [
            'foreignKey' => 'vehicle_type_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Intervals', [
            'foreignKey' => 'vehicle_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('company_id')
            ->notEmpty('company_id');

        $validator
            ->integer('vehicle_type_id')
            ->notEmpty('vehicle_type_id');

        $validator
            ->decimal('normal_minimum_value')
            ->requirePresence('normal_minimum_value', 'create')
            ->notEmpty('normal_minimum_value');

        $validator
            ->decimal('normal_km_value')
            ->requirePresence('normal_km_value', 'create')
            ->notEmpty('normal_km_value');

        $validator
            ->decimal('express_minimum_value')
            ->requirePresence('express_minimum_value', 'create')
            ->notEmpty('express_minimum_value');

        $validator
            ->decimal('express_km_value')
            ->requirePresence('express_km_value', 'create')
            ->notEmpty('express_km_value');

        $validator
            ->boolean('consider_moving_collection')
            ->requirePresence('consider_moving_collection', 'create')
            ->notEmpty('consider_moving_collection');

        $validator
            ->boolean('is_available')
            ->requirePresence('is_available', 'create')
            ->notEmpty('is_available');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['company_id'], 'Companies'));
        $rules->add($rules->existsIn(['vehicle_type_id'], 'VehicleTypes'));

        return $rules;
    }
}
