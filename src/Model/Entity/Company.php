<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Company Entity
 *
 * @property int $id
 * @property string $slug
 * @property string $name
 * @property string $cnpj
 * @property string $email
 * @property string $cep
 * @property string $address
 * @property int $street_number
 * @property string $complement
 * @property string $neighborhood
 * @property int $city_id
 * @property string $contact_phone
 * @property string $cell_phone
 * @property \Cake\I18n\FrozenTime $initial_morning_hour
 * @property \Cake\I18n\FrozenTime $final_morning_hour
 * @property \Cake\I18n\FrozenTime $initial_afternoon_hour
 * @property \Cake\I18n\FrozenTime $final_afternoon_hour
 * @property string|resource $logo
 * @property int $is_active
 *
 * @property \App\Model\Entity\City $city
 */
class Company extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => false,
        'slug' => false,
        'name' => true,
        'cnpj' => true,
        'email' => true,
        'cep' => true,
        'address' => true,
        'street_number' => true,
        'complement' => true,
        'neighborhood' => true,
        'city_id' => true,
        'contact_phone' => true,
        'cell_phone' => true,
        'initial_morning_hour' => true,
        'final_morning_hour' => true,
        'initial_afternoon_hour' => true,
        'final_afternoon_hour' => true,
        'logo' => true,
        'logo_dir' => true,
        'is_active' => true,
        'city' => true
    ];
}
