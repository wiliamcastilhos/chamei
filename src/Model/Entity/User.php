<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property int $roles_id
 * @property string $name
 * @property string $email
 * @property string $cep
 * @property string $address
 * @property string $street_number
 * @property string $complement
 * @property string $neighborhood
 * @property int $city_id
 * @property string $contact_phone
 * @property string $cell_phone
 * @property string $password
 *
 * @property \App\Model\Entity\Role $role
 * @property \App\Model\Entity\City $city
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'roles_id' => true,
        'name' => true,
        'email' => true,
        'cep' => true,
        'address' => true,
        'street_number' => true,
        'complement' => true,
        'neighborhood' => true,
        'city_id' => true,
        'contact_phone' => true,
        'cell_phone' => true,
        'password' => true,
        'role' => true,
        'city' => true,
        'is_active' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];

    protected function _setPassword($password){
        if(strlen($password) > 0){
            return (new DefaultPasswordHasher)->hash($password);
        }
    }
}
