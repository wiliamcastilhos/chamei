<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Calendar Entity
 *
 * @property int $id
 * @property int $company_id
 * @property int $year
 * @property \Cake\I18n\FrozenDate $date
 * @property \Cake\I18n\FrozenTime $initial_morning_hour
 * @property \Cake\I18n\FrozenTime $final_morning_hour
 * @property \Cake\I18n\FrozenTime $initial_afternoon_hour
 * @property \Cake\I18n\FrozenTime $final_afternoon_hour
 * @property bool $is_util_day
 *
 * @property \App\Model\Entity\Company $company
 */
class Calendar extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'company_id' => true,
        'year' => true,
        'date' => true,
        'initial_morning_hour' => true,
        'final_morning_hour' => true,
        'initial_afternoon_hour' => true,
        'final_afternoon_hour' => true,
        'is_util_day' => true,
        'company' => true
    ];
}
