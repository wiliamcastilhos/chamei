<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Interval Entity
 *
 * @property int $id
 * @property int $distance_from
 * @property int $distance_to
 * @property float $normal_value
 * @property float $express_value
 * @property int $vehicle_id
 *
 * @property \App\Model\Entity\Vehicle $vehicle
 */
class Interval extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'distance_from' => true,
        'distance_to' => true,
        'normal_value' => true,
        'express_value' => true,
        'vehicle_id' => true,
        'vehicle' => true
    ];
}
