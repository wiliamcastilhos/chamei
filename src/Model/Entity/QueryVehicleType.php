<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * QueryVehicleType Entity
 *
 * @property int $id
 * @property int $query_id
 * @property int $vehicle_type_id
 *
 * @property \App\Model\Entity\Query $query
 * @property \App\Model\Entity\VehicleType $vehicle_type
 */
class QueryVehicleType extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'query_id' => true,
        'vehicle_type_id' => true,
        'query' => true,
        'vehicle_type' => true
    ];
}
