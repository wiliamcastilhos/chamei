<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Vehicle Entity
 *
 * @property int $id
 * @property int $company_id
 * @property int $vehicle_type_id
 * @property float $normal_minimum_value
 * @property float $normal_km_value
 * @property float $express_minimum_value
 * @property float $express_km_value
 * @property bool $consider_moving_collection
 * @property bool $is_available
 *
 * @property \App\Model\Entity\Company $company
 * @property \App\Model\Entity\VehicleType $vehicle_type
 * @property \App\Model\Entity\Interval[] $intervals
 */
class Vehicle extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'company_id' => true,
        'vehicle_type_id' => true,
        'normal_minimum_value' => true,
        'normal_km_value' => true,
        'express_minimum_value' => true,
        'express_km_value' => true,
        'consider_moving_collection' => true,
        'is_available' => true,
        'company' => true,
        'vehicle_type' => true,
        'intervals' => true
    ];
}
