<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * QuerySolicitation Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $company_id
 * @property int $query_id
 * @property \Cake\I18n\FrozenDate $date
 * @property float $normal_value
 * @property float $express_value
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Company $company
 * @property \App\Model\Entity\Query $query
 */
class QuerySolicitation extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'company_id' => true,
        'query_id' => true,
        'date' => true,
        'normal_value' => true,
        'express_value' => true,
        'user' => true,
        'company' => true,
        'query' => true
    ];
}
