<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Query Entity
 *
 * @property int $id
 * @property int $city_collection_id
 * @property int $city_delivery_id
 * @property \Cake\I18n\FrozenDate $date
 * @property float $normal_value
 * @property float $express_value
 * @property string $collection_address
 * @property string $collection_street_number
 * @property string $collection_neighborhood
 * @property int $collection_cep
 * @property string $collection_complement
 * @property string $collection_references
 * @property string $delivery_address
 * @property string $delivery_street_number
 * @property string $delivery_neighborhood
 * @property int $delivery_cep
 * @property string $delivery_complement
 * @property string $delivery_references
 *
 * @property \App\Model\Entity\CityCollection $city_collection
 * @property \App\Model\Entity\CityDelivery $city_delivery
 * @property \App\Model\Entity\QuerySolicitation[] $query_solicitations
 */
class Query extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'city_collection_id' => true,
        'city_delivery_id' => true,
        'date' => true,
        'normal_value' => true,
        'express_value' => true,
        'collection_address' => true,
        'collection_street_number' => true,
        'collection_neighborhood' => true,
        'collection_cep' => true,
        'collection_complement' => true,
        'collection_references' => true,
        'delivery_address' => true,
        'delivery_street_number' => true,
        'delivery_neighborhood' => true,
        'delivery_cep' => true,
        'delivery_complement' => true,
        'delivery_references' => true,
        'city_collection' => true,
        'city_delivery' => true,
        'query_solicitations' => true
    ];
}
