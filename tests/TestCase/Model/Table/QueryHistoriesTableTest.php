<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\QueryHistoriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\QueryHistoriesTable Test Case
 */
class QueryHistoriesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\QueryHistoriesTable
     */
    public $QueryHistories;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.query_histories',
        'app.companies',
        'app.city_collections',
        'app.city_deliveries',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('QueryHistories') ? [] : ['className' => QueryHistoriesTable::class];
        $this->QueryHistories = TableRegistry::get('QueryHistories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->QueryHistories);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
