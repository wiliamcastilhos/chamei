<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\QueryVehicleTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\QueryVehicleTypesTable Test Case
 */
class QueryVehicleTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\QueryVehicleTypesTable
     */
    public $QueryVehicleTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.query_vehicle_types',
        'app.queries',
        'app.vehicle_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('QueryVehicleTypes') ? [] : ['className' => QueryVehicleTypesTable::class];
        $this->QueryVehicleTypes = TableRegistry::get('QueryVehicleTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->QueryVehicleTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
