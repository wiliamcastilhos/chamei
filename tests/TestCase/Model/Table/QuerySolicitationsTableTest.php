<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\QuerySolicitationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\QuerySolicitationsTable Test Case
 */
class QuerySolicitationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\QuerySolicitationsTable
     */
    public $QuerySolicitations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.query_solicitations',
        'app.users',
        'app.companies',
        'app.queries'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('QuerySolicitations') ? [] : ['className' => QuerySolicitationsTable::class];
        $this->QuerySolicitations = TableRegistry::get('QuerySolicitations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->QuerySolicitations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
