<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\IntervalsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\IntervalsTable Test Case
 */
class IntervalsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\IntervalsTable
     */
    public $Intervals;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.intervals',
        'app.vehicles',
        'app.companies',
        'app.cities',
        'app.states',
        'app.vehicle_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Intervals') ? [] : ['className' => IntervalsTable::class];
        $this->Intervals = TableRegistry::get('Intervals', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Intervals);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
