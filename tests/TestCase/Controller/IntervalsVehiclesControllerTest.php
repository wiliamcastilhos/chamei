<?php
namespace App\Test\TestCase\Controller;

use App\Controller\IntervalsVehiclesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\IntervalsVehiclesController Test Case
 */
class IntervalsVehiclesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.intervals_vehicles',
        'app.vehicles',
        'app.companies',
        'app.cities',
        'app.states',
        'app.vehicle_types',
        'app.intervals'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
