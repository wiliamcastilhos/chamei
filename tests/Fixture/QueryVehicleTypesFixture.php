<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * QueryVehicleTypesFixture
 *
 */
class QueryVehicleTypesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'query_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'vehicle_type_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'query_id' => ['type' => 'index', 'columns' => ['query_id'], 'length' => []],
            'vehicle_type_id' => ['type' => 'index', 'columns' => ['vehicle_type_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'query_vehicle_types_ibfk_1' => ['type' => 'foreign', 'columns' => ['query_id'], 'references' => ['queries', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'query_vehicle_types_ibfk_2' => ['type' => 'foreign', 'columns' => ['vehicle_type_id'], 'references' => ['vehicle_types', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'query_id' => 1,
                'vehicle_type_id' => 1
            ],
        ];
        parent::init();
    }
}
