function loadCities(){
    $(".loading").toggle(50,function(){$("#city-id").attr("visibility","hidden");});
    var dataSet = {stateId: $("#states-id").val()};
    var requestUrl = "/myapp/cities/list";
    $.ajax({
        type: "POST",
        url: requestUrl,
        data: dataSet,
        success: function(result) {
            $("#city-id").html(result).removeClass("hiddenVisibility");
            $(".loading").toggle(50,function(){$("#city-id").attr("visibility","visible");});
            selectCity();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
}

function selectCity(){
    if($("input[name=cityIdSelected]").val() != "" && $("input[name=cityIdSelected]").val() != undefined){
        $("#city-id > option").each(function() {
            if(this.value == $("input[name=cityIdSelected]").val()){
                $(this).attr("selected","selected");
                $("#city-id").trigger("change");
            }
        });
    }
}

$(function() {
  if($("input[name=stateIdSelected]").val() != "" && $("input[name=stateIdSelected]").val() != undefined){
        $("#states-id > option").each(function() {
            if(this.value == $("input[name=stateIdSelected]").val()){
                $(this).attr("selected","selected");
                $("#states-id").trigger("change");
            }
        });
    }

    if($("#states-id").val() != '' && $("#city-id").val() == ''){
        loadCities();
    }

    if($("#cnpj").val() != undefined){
        $("#cnpj").mask("00.000.000/0000-00", {placeholder: "__.___.___/____-__"});
    }

    if($("#cep").val() != undefined){
        $("#cep").blur(function(){
            requestAdrress();
        });
        $("#cep").mask("00000-000", {placeholder: "_____-___"});
    }

    if($("#contact-phone").val() != undefined){
        $("#contact-phone").mask("(00) 0000-0000",{placeholder: "(__) ____-____"});
    }

    if($("#cell-phone").val() != undefined){
        var MaskBehavior = function (val) {
          return val.replace(/\D/g, "").length === 11 ? "(00) 00000-0000" : "(00) 00000-0009";
        },
        Options = {
          onKeyPress: function(val, e, field, options) {
              field.mask(MaskBehavior.apply({}, arguments), options);
            },
            placeholder: "(__) ____-_____"
        }; 
        $("#cell-phone").mask(MaskBehavior, Options);
    }
});

function requestAdrress(){
    var cep = $("#cep").val().replace(/[.-]+/g,"");
    if(cep != undefined && cep != ""){
        $.ajax({
        type: "POST",
        url: "http://viacep.com.br/ws/"+cep+"/json/",
        success: function(result) {
            loadAdrress(result);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
    }
}

function loadAdrress(json){
    var address = json.logradouro;   
    var complement = json.complemento;
    var neighborhood = json.bairro;
    var nmCity = json.localidade;
    var nmUf = json.uf;
    var requestUrl = "/myapp/cities/findStateAndCity";
    var dataSet = { nmUf: nmUf , nmCity: nmCity};

    $.ajax({
        type: "POST",
        url: requestUrl,
        data: dataSet,
        success: function(result) {
            $(".loading").toggle(50,function(){$("#city-id").attr("visibility","hidden");});
            $("#states-id").html(result.states);
            $("#city-id").html(result.cities).removeClass("hiddenVisibility");
            $(".loading").toggle(50,function(){$("#city-id").attr("visibility","visible");});
            $("#address").val(address);
            $("#neighborhood").val(neighborhood);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
}